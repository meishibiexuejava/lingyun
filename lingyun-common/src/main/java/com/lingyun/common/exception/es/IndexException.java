package com.lingyun.common.exception.es;

import com.lingyun.common.utils.MessageUtils;

public class IndexException extends ElasticSearchException{


    private static final long serialVersionUID = 1L;

    public IndexException() {
        super(MessageUtils.message("index.not.exists"));
    }

}
