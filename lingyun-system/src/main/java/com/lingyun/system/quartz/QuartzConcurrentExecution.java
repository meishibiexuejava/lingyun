package com.lingyun.system.quartz;

import com.lingyun.common.pojo.LyJob;
import com.lingyun.common.utils.quartz.AbstractQuartzJob;
import com.lingyun.common.utils.quartz.JobInvokeUtil;
import org.quartz.JobExecutionContext;

/**
 * 允许并发执行
 */
public class QuartzConcurrentExecution extends AbstractQuartzJob {

    @Override
    protected void doExecute(JobExecutionContext context, LyJob job) throws Exception {
        JobInvokeUtil.invokeMethod(job);
    }
}
