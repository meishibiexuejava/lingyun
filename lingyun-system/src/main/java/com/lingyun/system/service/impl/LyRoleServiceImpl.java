package com.lingyun.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lingyun.common.annotation.DataJurisdiction;
import com.lingyun.common.corebase.GeneralEntity;
import com.lingyun.common.exception.role.RoleHaveUserCanNotDelException;
import com.lingyun.common.mapper.LyRoleMapper;
import com.lingyun.common.mapper.LyRoleMenuMapper;
import com.lingyun.common.mapper.LyUserRoleMapper;
import com.lingyun.common.pojo.LyRole;
import com.lingyun.common.pojo.LyRoleMenu;
import com.lingyun.common.pojo.LyUser;
import com.lingyun.common.pojo.LyUserRole;
import com.lingyun.common.vo.role.AllocationUserCondition;
import com.lingyun.common.vo.role.RoleVagueQuery;
import com.lingyun.common.vo.user.UserQueryCriteria;
import com.lingyun.system.service.LyRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 没事别学JAVA
 * 2021/12/16 0:57
 */
@Service("lyRoleService")
public class LyRoleServiceImpl extends ServiceImpl<LyRoleMapper, LyRole> implements LyRoleService {

    @Resource
    LyRoleMapper lyRoleMapper;

    @Resource
    LyRoleMenuMapper lyRoleNavMapper;

    @Resource
    LyUserRoleMapper lyUserRoleMapper;


    @DataJurisdiction
    @Override
    public IPage<LyRole> roleList(GeneralEntity generalEntity) {
        QueryWrapper<LyRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("ly_role.del_flag", 0);
        if (!StrUtil.hasEmpty(generalEntity.getRoleName())) {
            queryWrapper.like("ly_role.role_name", generalEntity.getRoleName());
        }
        if (!StrUtil.hasEmpty(generalEntity.getRoleKey())) {
            queryWrapper.like("ly_role.role_key", generalEntity.getRoleKey());
        }
        if (!StrUtil.hasEmpty(generalEntity.getRoleStatus())) {
            queryWrapper.eq("ly_role.role_status", generalEntity.getRoleStatus());
        }
        Page<LyRole> page = new Page<>(generalEntity.getCurrent(), generalEntity.getSize());

        IPage<LyRole> iPage = lyRoleMapper.roleList(page, queryWrapper, generalEntity);
        return iPage;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer addRole(LyRole lyRole) {
        Date date = new Date();
        //角色创建时间
        lyRole.setCreateTime(date);
        //角色更新时间
        lyRole.setUpdateTime(date);
        //角色信息入库
        lyRoleMapper.insert(lyRole);
        // 保存新的角色和菜单关联
        return insertRoleNav(lyRole);
    }


    @Override
    public LyRole selectRoleById(Long roleId) {
        return lyRoleMapper.selectRoleById(roleId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer updateRole(LyRole lyRole) {
        // 修改角色 更新时间
        lyRole.setUpdateTime(new Date());
        UpdateWrapper<LyRole> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("ly_role.role_id", lyRole.getRoleId());
        lyRoleMapper.update(lyRole, updateWrapper);
        // 删除原有的角色和菜单的关联
        lyRoleNavMapper.deleteRoleNavByRoleId(lyRole.getRoleId());
        // 保存新的角色和菜单关联
        return insertRoleNav(lyRole);
    }

    /**
     * 批量保存 角色 菜单 关系表
     *
     * @param lyRole 角色信息
     * @return 结果
     */
    public Integer insertRoleNav(LyRole lyRole) {
        List<LyRoleMenu> lyRoleNavList = new ArrayList<>();
        for (Long navid : lyRole.getMenuIds()) {
            LyRoleMenu lyRoleNav = new LyRoleMenu();
            lyRoleNav.setRoleId(lyRole.getRoleId());
            lyRoleNav.setMenuId(navid);
            lyRoleNavList.add(lyRoleNav);
        }
        Integer rows = 1;
        if (lyRoleNavList.size() > 0) {
            rows = lyRoleNavMapper.batchRoleNav(lyRoleNavList);
        }
        return rows;
    }

    @Override
    public boolean deleteRoleById(Long roleId) throws RoleHaveUserCanNotDelException {
        if (lyUserRoleMapper.selectRoleHaveHowmanyUser(roleId) != 0) { // 查看角色是否拥有角色，如果有就不能删除
            throw new RoleHaveUserCanNotDelException();
        } else {
            if (lyRoleMapper.deleteById(roleId) > 0) {
                return true;
            }
        }
        return false;
    }


    @Override
    public List<LyRole> roleExportExcel(GeneralEntity generalEntity) {
        QueryWrapper<LyRole> queryWrapper = new QueryWrapper<>();
        if (!StrUtil.hasEmpty(generalEntity.getRoleName())) {
            queryWrapper.like("ly_role.role_name", generalEntity.getRoleName());
        }
        if (!StrUtil.hasEmpty(generalEntity.getRoleKey())) {
            queryWrapper.like("ly_role.role_key", generalEntity.getRoleKey());
        }
        if (!StrUtil.hasEmpty(generalEntity.getRoleStatus())) {
            queryWrapper.eq("ly_role.role_status", generalEntity.getRoleStatus());
        }
        queryWrapper.orderByAsc("ly_role.role_id");
        return lyRoleMapper.roleExportExcel(queryWrapper);
    }

    @Override
    public IPage<LyUser> selectRoleUser(AllocationUserCondition allocationUserCondition) {
        QueryWrapper<LyUser> queryWrapper = new QueryWrapper<>();
        if (!StrUtil.hasEmpty(allocationUserCondition.getUserName())) {
            queryWrapper.like("u.user_name", allocationUserCondition.getUserName());
        }
        if (!StrUtil.hasEmpty(allocationUserCondition.getNickName())) {
            queryWrapper.like("u.nick_name", allocationUserCondition.getNickName());
        }
        queryWrapper.eq("r.role_id", allocationUserCondition.getRoleId());
        queryWrapper.orderByAsc("u.user_id");
        Page<LyUser> page = new Page<>(allocationUserCondition.getCurrent(), allocationUserCondition.getSize());
        IPage<LyUser> iPage = lyRoleMapper.selectRoleUser(page, queryWrapper);
        return iPage;
    }

    @Override
    public Boolean cancelBinding(LyUserRole lyUserRole) {
        return lyUserRoleMapper.cancelBinding(lyUserRole) > 0;
    }


    @Override
    public Boolean batchCancelBinding(Long roleId, Long[] userIds) {
        return lyUserRoleMapper.batchCancelBinding(roleId, userIds) > 0;
    }

    @Override
    public Boolean bindingUser(Long roleId, Long[] userIds) {
        List<LyUserRole> userRoles = new ArrayList<>();
        for (Long userId : userIds) {
            LyUserRole userRole = new LyUserRole();
            userRole.setUserId(userId);
            userRole.setRoleId(roleId);
            if (lyUserRoleMapper.queryBindingIsNoRepeat(userRole) == 0) { // 判断是否重复，0为没有，添加新的关联关系0
                userRoles.add(userRole);
            }
        }
        if (userRoles.size() == 0) {
            return true;
        }
        return lyUserRoleMapper.bindingUser(userRoles) > 0;
    }


    @Override
    public List<LyRole> selectOption() {
        QueryWrapper<LyRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("role_id");
        return lyRoleMapper.selectList(queryWrapper);
    }

}
