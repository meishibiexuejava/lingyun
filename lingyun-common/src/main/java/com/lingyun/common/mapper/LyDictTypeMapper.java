package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.LyDictType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (LyDictType)
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:23:17
 */
public interface LyDictTypeMapper extends BaseMapper<LyDictType> {


    /**
     * 根据条件分页查询字典类型列表
     * @param page 分页
     * @param queryWrapper 查询条件
     * @return 字典类型列表
     */
    public IPage<LyDictType> dictList(IPage<LyDictType> page, @Param("ew") Wrapper<LyDictType> queryWrapper);



    /**
     * 根据字典类型id 获取字典数据
     * @param dictId 字典类型id
     * @return 字典类型信息
     */
    public LyDictType selectDictById(Long dictId);


    /**
     * 获取字典类型名称---下拉框使用
     * @return  字典类型列表
     */
    public List<LyDictType> selectDictTypeAll();

}
