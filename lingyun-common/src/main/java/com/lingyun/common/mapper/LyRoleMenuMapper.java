package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lingyun.common.pojo.LyRoleMenu;

import java.util.List;

/**
 * @author 没事别学JAVA
 */
public interface LyRoleMenuMapper extends BaseMapper<LyRoleMenu> {

    public Integer deleteRoleNavByRoleId(Long RoleId);

    /**
     * 批量保存 角色 菜单 关系表
     *
     * @param lyRoleNavList 角色和菜单 关联信息
     * @return 结果
     */
    public Integer batchRoleNav(List<LyRoleMenu> lyRoleNavList);

}
