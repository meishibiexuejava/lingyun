import {login, logout} from "@/api/admin/login";

import {getToken, setToken, removeToken} from "@/utils/cookie";

const user = {

    state: {
        token: getToken(),
    },

    mutations: {
        SET_TOKEN: (state, token) => {
            state.token = token
        },
    },


    actions: {

        // 登录
        Login({commit}, loginForm) {
            return new Promise((resolve, reject) => {
                login(loginForm).then(res => {
                    setToken(res.token)
                    commit('SET_TOKEN', res.token)
                    resolve() // 如果操作成功，调用 resolve 回调函数
                }).catch(error => {
                    reject(error) // 如果操作失败，调用 reject 回调函数
                })
            })
        },

        // 退出系统
        LogOut({commit, state}) {
            return new Promise((resolve, reject) => {
                logout(state.token).then(() => {
                    removeToken()
                    commit('SET_TOKEN', '')
                    resolve()
                }).catch(error => {
                    reject(error)
                })
            })
        },


    }


}
export default user
