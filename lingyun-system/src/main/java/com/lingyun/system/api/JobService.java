package com.lingyun.system.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.corebase.GeneralEntity;
import com.lingyun.common.pojo.JobAndTrigger;
import com.lingyun.common.pojo.JobFrom;
import org.quartz.SchedulerException;

/**
 * @author 没事别学JAVA
 * 2021/4/27 17:56
 */
public interface JobService {

    /**
     * 获取定时任务列表
     * @param generalEntity 基本列表搜索条件类
     * @return 获取定时任务列表
     */
    IPage<JobAndTrigger> quartzList(GeneralEntity generalEntity);

    /**
     * 添加定时任务
     * @param job 定时任务信息
     * @throws SchedulerException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void quartzAdd(JobFrom job) throws SchedulerException, ClassNotFoundException, InstantiationException, IllegalAccessException;

    /**
     * 删除定时任务
     * @param job 定时任务信息
     * @throws Exception
     */
    void deleteJob(JobFrom job) throws Exception;

}
