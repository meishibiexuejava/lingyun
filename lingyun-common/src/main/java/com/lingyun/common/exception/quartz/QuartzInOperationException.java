package com.lingyun.common.exception.quartz;

import com.lingyun.common.utils.MessageUtils;

/**
 * 定时任务运行中异常
 */
public class QuartzInOperationException extends QuartzException {
    private static final long serialVersionUID = 1L;

    public QuartzInOperationException() {
        super(MessageUtils.message("quartz.in.operation"));
    }

}
