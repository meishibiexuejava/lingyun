package com.lingyun.framework.security;

import com.alibaba.fastjson.JSON;
import com.lingyun.common.utils.AjaxResult;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 用户登录成功时返回前端数据
 *
 * @author 没事别学JAVA
 *
 */
@Component
public class AjaxAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        System.out.println("用户登录成功");
        httpServletResponse.setHeader("Content-type", "application/json; charset=utf-8");
        httpServletResponse.getWriter().write(JSON.toJSONString(AjaxResult.success("用户登录成功")));
    }

}
