package com.lingyun.common.pojo;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@ApiModel(value = "角色菜单关系表",description = "角色菜单关系表")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LyRoleMenu implements Serializable {

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色ID")
    private Long roleId;

    /**
     * 菜单id
     */
    @ApiModelProperty(value = "菜单ID")
    private Long menuId;
}
