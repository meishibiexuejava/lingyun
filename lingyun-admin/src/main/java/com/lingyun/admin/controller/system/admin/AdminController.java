package com.lingyun.admin.controller.system.admin;

import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.utils.ip.IpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 首页
 *
 * @author meishibiexuejava
 * 2023/11/26 22:53
 */


@RestController
@RequestMapping("/system/admin")
@Api(value = "首页", tags = "首页")
public class AdminController {

    /**
     * 获取客户端ip
     */
    @ApiOperation(value = "获取客户端ip")
    @GetMapping("/ip")
    public AjaxResult ip(){

        IpUtils.getClientIp(null);

        return AjaxResult.success();
    }


    @ApiOperation(value = "获取客户端地区")
    @GetMapping("/address")
    public AjaxResult address(){

        return AjaxResult.success();
    }

    @ApiOperation(value = "获取用户信息：账号（名称）、角色、上次登录时间、本次登录时间")
    @GetMapping("/userInfo")
    public AjaxResult userInfo(){
        return AjaxResult.success();
    }






}
