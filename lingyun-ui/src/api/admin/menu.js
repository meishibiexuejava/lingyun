import request from '@/utils/request'

/**
 * 获取左侧导航栏信息
 * @returns {*}
 */
export function getRouters() {

  return request({
    url: '/system/menu/getAllRouters',
    method: 'get'
  })

}


/**
 * 1、获取当前登录角色的所拥有的菜单权限 2、根据角色id获取修改的角色的菜单id（角色修改页面使用）
 *
 * @param roleId 角色id
 * @returns {*}
 */
export function roleMenuTreeselect(roleId) {

  return request({
    url: '/system/menu/roleMenuTress/' + roleId,
    method: 'get'
  })

}


/**
 * 获取当前角色所拥有的菜单（添加角色页面使用）
 * @returns {*}
 */
export function menuTreeselect(){

  return request({
    url: '/system/menu/treeSelect',
    method: 'get'
  })

}









/**
 * 查询菜单列表
 * @returns {*}
 */
export function queryMenuList(){

  return request({
    url: '/system/menu/queryMenuList',
    method: 'get'
  })

}

/**
 * 添加菜单
 * @param data 菜单信息
 * @returns {*}
 */
export function addMenu(data){

  return request({
    url: '/system/menu/addMenu',
    method: 'post',
    data: data
  })

}

/**
 * 根据菜单id查询菜单信息
 * @param menuId 菜单id
 * @returns {*} 菜单信息
 */
export function getMenuById(menuId){

  return request({
    url: '/system/menu/getMenuById/'+menuId,
    method: 'get',
  })

}

/**
 * 修改菜单
 * @param data 菜单信息
 * @returns {*}
 */
export function updateMenu(data){

  return request({
    url: '/system/menu/updateMenu',
    method: 'put',
    data: data
  })

}

/**
 * 删除菜单
 * @param menuId 菜单id
 * @returns {*}
 */
export function deleteMenu(menuId){

  return request({
    url: '/system/menu/deleteMenu/'+menuId,
    method: 'delete',
  })

}


/**
 * 根据字典类型查询字典数据（全局使用vue）
 * @param dictType 字典类型
 * @return {AxiosPromise}
 */
export function getDicts(dictType) {
  return request({
    url: '/system/dict/queryDictDataByType/' + dictType,
    method: 'get'
  })
}
