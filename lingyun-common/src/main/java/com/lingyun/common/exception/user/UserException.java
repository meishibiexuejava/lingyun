package com.lingyun.common.exception.user;

import com.lingyun.common.exception.LyException;

/**
 * 用户信息异常类
 *
 * @author ruoyi
 */
public class UserException extends LyException {
    private static final long serialVersionUID = 1L;

    public UserException(String message) {
        super("user", message, null);
    }
}
