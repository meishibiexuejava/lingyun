import request from '@/utils/request'


/**
 * 根据条件分页查询日志列表
 * @param query 日志筛选条件
 * @return {AxiosPromise}
 */
export function logList(query) {

    return request({
        url: '/system/log/logList',
        method: 'get',
        params: query
    })

}


/**
 * 查看日志详情
 * @param logId 日志id
 * @return {*}
 */
export function getLogById(logId){

    return request({
        url: '/system/log/getLogById/'+logId,
        method: 'get',
    })

}
