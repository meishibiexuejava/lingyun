package com.lingyun.common.exception;

/**
 * 系统异常
 * @author 没事别学JAVA
 * 2021/12/25 1:52
 */
public class ServiceException extends RuntimeException{

    public ServiceException(String message) {
        super(message);
    }

}
