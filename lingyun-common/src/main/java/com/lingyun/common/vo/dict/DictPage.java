package com.lingyun.common.vo.dict;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@ApiModel(value = "DictPage", description = "字典筛选条件")
public class DictPage {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private Integer current;
    /**
     * 页大小
     */
    @ApiModelProperty(value = "页大小", required = true)
    private Integer size;
    /**
     * 字典名称
     */
    @ApiModelProperty(value = "字典名称")
    private String dictName;
    /**
     * 字典类型
     */
    @ApiModelProperty(value = "字典类型")
    private String dictType;
    /**
     * 状态（0正常 1停用）
     */
    @ApiModelProperty(value = "状态")
    private String status;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private String[] dateTimeRangeCreate;
    /**
     * 更新时间范围
     */
    @ApiModelProperty(value = "更新时间")
    private String[] dateTimeRangeUpdate;

}
