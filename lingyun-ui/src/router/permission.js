import router from '@/router'
import store from '@/store'
import {getToken} from "@/utils/cookie";
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

NProgress.configure({ showSpinner: false })
router.beforeEach((to, from, next) => {
    console.log('前置路由守卫', to, from)
    NProgress.start()
    if(to.meta.isAuth){
        if(getToken()){
            if(to.path === '/login'){
                next({ path: '/admin' })
                NProgress.done()
            }else{
                store.dispatch('GenerateRoutes').then(res =>{
                    next()
                })
            }
        }else{
            if(to.path === '/login'){
                next()
            }else{
                next({ path: '/login' }) // 否则全部重定向到登录页
                NProgress.done()
            }
        }
    }else{
        next()
        NProgress.done()
    }

})



router.afterEach((to, from) => {
    console.log('后置路由守卫', to, from)
    NProgress.done()
})
