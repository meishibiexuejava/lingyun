package com.lingyun.common.vo.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 用户筛选条件
 *
 * @author 没事别学JAVA
 * 2021/11/16 1:18
 */
@Data
@ToString
@ApiModel(value = "UserQueryCriteria",description = "用户筛选条件")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserQueryCriteria implements Serializable{

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页",required = true)
    private Integer current;
    /**
     * 页大小
     */
    @ApiModelProperty(value = "页大小",required = true)
    private Integer size;

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String userName;
    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    private String nickName;

    /**
     * 最后登录ip
     */
    @ApiModelProperty(value = "最后登录ip")
    private String loginIp;

//    /**
//     * 角色id
//     */
//    @ApiModelProperty(value = "角色id")
//    private Long roleId;

}
