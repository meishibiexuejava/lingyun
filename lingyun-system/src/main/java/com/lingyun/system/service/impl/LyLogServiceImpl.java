package com.lingyun.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lingyun.common.mapper.LyLogMapper;
import com.lingyun.common.pojo.LyArticle;
import com.lingyun.common.pojo.LyLog;
import com.lingyun.common.vo.log.LogSearch;
import com.lingyun.system.service.LyLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 日志表(LyLog)表服务实现类
 *
 * @author 没事别学JAVA
 * @since 2023-05-07 22:56:35
 */
@Service("lyLogService")
public class LyLogServiceImpl extends ServiceImpl<LyLogMapper, LyLog> implements LyLogService {



    @Resource
    LyLogMapper lyLogMapper;

    @Override
    public IPage<LyLog> logList(LogSearch logSearch) {
        QueryWrapper<LyLog> queryWrapper = new QueryWrapper<>();
        if (!StrUtil.hasEmpty(logSearch.getLogTitle())) {
            queryWrapper.like("log_title", logSearch.getLogTitle());
        }
        if (!StrUtil.hasEmpty(logSearch.getLogBusinessType())) {
            queryWrapper.eq("log_business_type", logSearch.getLogBusinessType());
        }
        if (!StrUtil.hasEmpty(logSearch.getLogOperationStatus())) {
            queryWrapper.eq("log_operation_status", logSearch.getLogOperationStatus());
        }
        queryWrapper.orderByDesc("log_operation_date");  // 以日志操作时间排序
        Page<LyLog> page = new Page<>(logSearch.getCurrent(),logSearch.getSize());
        IPage<LyLog> iPage = lyLogMapper.lyLogList(page,queryWrapper);
        return iPage;
    }



}
