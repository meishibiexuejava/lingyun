package com.lingyun.common.vo.role;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * @author meishibiexuejava
 * 2023/4/9 0:36
 */
@Data
@ToString
@ApiModel(value = "AllocationUserCondition", description = "分配用户搜索条件")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AllocationUserCondition {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private Integer current;
    /**
     * 页大小
     */
    @ApiModelProperty(value = "页大小", required = true)
    private Integer size;

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String userName;
    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    private String nickName;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id", required = true)
    private Long roleId;

}
