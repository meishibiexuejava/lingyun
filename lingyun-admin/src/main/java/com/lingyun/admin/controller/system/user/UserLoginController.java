package com.lingyun.admin.controller.system.user;

import com.lingyun.common.annotation.Log;
import com.lingyun.common.exception.ServiceException;
import com.lingyun.common.exception.user.CaptchaException;
import com.lingyun.common.exception.user.CaptchaExpireException;
import com.lingyun.common.exception.user.UserAuthenticationException;
import com.lingyun.common.exception.user.UserPasswordNotMatchException;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.LoginFrom;
import com.lingyun.system.service.LyUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 后台登录
 *
 * @author 没事别学JAVA
 */
@RestController
@RequestMapping("/system")
@Api(value = "后台登录", tags = "后台登录")
public class UserLoginController {

    @Resource
    private LyUserService lyUserService;

    /**
     * 后台登录
     * @param loginFrom 登录信息
     * @param request
     * @return token
     */
    @ApiOperation(value = "后台登录")
    @Log(title = "登录模块", businessType = "10", description = "后台登录")
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginFrom loginFrom, HttpServletRequest request) {
        String token = null;
        try {
            token = lyUserService.login(loginFrom, request);
        } catch (CaptchaExpireException | CaptchaException | UserPasswordNotMatchException |
                 UserAuthenticationException | ServiceException e) {
            return AjaxResult.error(e.getMessage());
        }
        AjaxResult ajax = AjaxResult.success();
        ajax.put("token", token);
        return ajax;
    }

}
