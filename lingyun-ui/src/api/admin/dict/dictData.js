import request from '@/utils/request'

/**
 * 根据条件分页查询字典数据列表
 * @param query 字典数据筛选条件
 * @returns {*}
 */
export function dictDataList(query) {

    return request({
        url: '/system/dict/dictDataList',
        method: 'get',
        params: query
    })

}

/**
 * 添加字典数据
 * @param data 字典数据信息
 * @returns {*}
 */
export function addDictData(data) {

    return request({
        url: '/system/dict/addDictData',
        method: 'post',
        data: data
    })

}

/**
 * 根据字典数据编码查询字典数据
 * @param dictCode 字典编码
 * @returns {*}
 */
export function selectDictDataById(dictCode) {

    return request({
        url: '/system/dict/selectDictDataById/' + dictCode,
        method: 'get'
    })

}

/**
 * 修改字典数据
 * @param data 字典数据信息
 * @returns {*}
 */
export function updateDictData(data) {

    return request({
        url: '/system/dict/updateDictData',
        method: 'put',
        data: data
    })

}


/**
 * 删除字典数据
 * @param dictCode 字典编码
 * @returns {*}
 */
export function deleteDictDataById(dictCode) {

    return request({
        url: '/system/dict/deleteDictDataById/' + dictCode,
        method: 'delete'
    })

}


/**
 * 批量删除字典数据
 * @param ids 字典编码（list）
 * @returns {*}
 */
export function deleteDictDataByIds(ids) {

    return request({
        url: '/system/dict/deleteDictDataByIds',
        method: 'delete',
        data: ids
    })

}








