import request from '@/utils/request'

/**
 * 根据条件分页查询字典类型列表
 * @param query 字典筛选条件
 * @returns {*}
 */
export function dictList(query) {

    return request({
        url: '/system/dict/dictList',
        method: 'get',
        params: query
    })

}

/**
 * 添加字典类型
 * @param data  字典类型信息
 */
export function addDictType(data) {

    return request({
        url: '/system/dict/addDictType',
        method: 'post',
        data: data
    })

}

/**
 * 根据字典类型id查询字典类型信息
 * @param dictId 字典类型id
 * @returns {*}
 */
export function selectDictById(dictId) {

    return request({
        url: '/system/dict/selectDictByID/' + dictId,
        method: 'get',
    })

}

/**
 * 修改字典类型
 * @param data  字典类型信息
 */
export function updateDicType(data) {

    return request({
        url: '/system/dict/updateDictType',
        method: 'put',
        data: data
    })

}

/**
 * 删除字典类型
 *
 * @param dictId  字典类型id
 * @returns {*}
 */
export function deleteDictTypeById(dictId) {

    return request({
        url: '/system/dict/deleteDictTypeById/' + dictId,
        method: 'delete',
    })

}

/**
 * 批量删除字典类型
 *
 * @param ids  字典类型id（list）
 * @returns {*}
 */
export function deleteDictTypeByIds(ids) {

    return request({
        url: '/system/dict/deleteDictTypeByIds',
        method: 'delete',
        data: ids
    })

}

/**
 * 获取字典类型名称(下拉框使用)
 * @returns {*}
 */
export function selectDictTypeAll() {

    return request({
        url: '/system/dict/selectDictTypeAll',
        method: 'get',
    })

}






