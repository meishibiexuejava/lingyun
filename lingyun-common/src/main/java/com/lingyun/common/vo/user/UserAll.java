package com.lingyun.common.vo.user;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;
import java.io.Serializable;


/**
 * 返回的查询全部用户信息
 * @author 没事别学JAVA
 * 2021/11/14 22:46
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserAll implements Serializable {
    /**
     * 用户ID
     */
    @Excel(name = "用户ID")
    private Long userId;
    /**
     * 账号
     */
    @Excel(name = "账号")
    private String userName;
    /**
     * 昵称
     */
    @Excel(name = "昵称")
    private String nickName;
    /**
     * 邮箱
     */
    @Excel(name = "邮箱")
    private String userEmail;
    /**
     * 手机号码
     */
    @Excel(name = "手机号码")
    private String phonenumber;
    /**
     * 性别 （0男 1女 2未知）
     */
    @Excel(name = "性别 （0男 1女 2未知）", replace = {"男生_0","女生_1","未知_2"})
    private String userSex;
    /**
     * 头像路径
     */
    @Excel(name = "用户头像",type = 2 ,width = 40 , height = 40, imageType = 1)
    private String avatar;
    /**
     * 账号状态（0正常 1停用）
     */
    @Excel(name = "账号状态（0正常 1停用）", replace = {"正常_0","停用_1"})
    private String userStatus;
    /**
     * 最后登录ip
     */
    @Excel(name = "最后登录ip")
    private String loginIp;
}
