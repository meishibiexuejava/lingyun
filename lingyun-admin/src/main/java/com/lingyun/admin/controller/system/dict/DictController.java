package com.lingyun.admin.controller.system.dict;

import com.lingyun.common.annotation.Log;
import com.lingyun.common.exception.dict.DictHaveDataException;
import com.lingyun.common.pojo.LyDictData;
import com.lingyun.common.pojo.LyDictType;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.dict.DictDataPage;
import com.lingyun.common.vo.dict.DictPage;
import com.lingyun.system.service.LyDictDataService;
import com.lingyun.system.service.LyDictTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 字典管理
 *
 * @author 没事别学JAVA
 */
@RestController
@RequestMapping("/system/dict")
@Api(value = "字典管理", tags = "字典管理")
public class DictController {

    @Resource
    LyDictTypeService lyDictTypeService;

    @Resource
    LyDictDataService lyDictDataService;


    /**
     * 根据条件分页查询字典类型列表
     *
     * @param dictPage 字典筛选条件
     * @return 结果
     */
    @ApiOperation(value = "根据条件分页查询字典类型列表")
    @Log(title = "字典模块", businessType = "0", description = "根据条件分页查询字典类型列表")
    @PreAuthorize("hasRole('system:dict:dictlist')")
    @GetMapping("/dictList")
    public AjaxResult dictList(DictPage dictPage) {
        return AjaxResult.success("查询成功", lyDictTypeService.dictList(dictPage));
    }

    /**
     * 添加字典类型
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    @ApiOperation(value = "添加字典类型")
    @Log(title = "字典模块", businessType = "1", description = "添加字典类型")
    @PreAuthorize("hasRole('system:dict:adddicttype')")
    @PostMapping("/addDictType")
    public AjaxResult addDictType(@RequestBody LyDictType dictType) {
        dictType.setCreateTime(new Date());
        dictType.setUpdateTime(new Date());
        if (lyDictTypeService.save(dictType)) {
            return AjaxResult.success("添加成功");
        }
        return AjaxResult.success("添加失败");
    }

    /**
     * 根据字典类型id查询字典类型信息
     *
     * @param dictId 字典类型id
     * @return 字典类型信息
     */
    @ApiOperation(value = "根据字典类型id查询字典类型信息")
    @ApiImplicitParam(name = "dictId", value = "字典类型id", required = true)
    @PreAuthorize("hasRole('system:dict:selectdictbyid')")
    @GetMapping("/selectDictByID/{dictId}")
    public AjaxResult selectDictByID(@PathVariable Long dictId) {
        return AjaxResult.success("查询成功", lyDictTypeService.selectDictById(dictId));
    }

    /**
     * 修改字典类型
     *
     * @param dictType 字典类型信息
     * @return 结果
     */
    @ApiOperation(value = "修改字典类型")
    @Log(title = "字典模块", businessType = "2", description = "修改字典类型")
    @PreAuthorize("hasRole('system:dict:updatedicttype')")
    @PutMapping("/updateDictType")
    public AjaxResult updateDictType(@RequestBody LyDictType dictType) {
        dictType.setUpdateTime(new Date());
        if (lyDictTypeService.updateById(dictType)) {
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.success("修改失败");
    }


    /**
     * 删除字典类型
     *
     * @param dictId 字典类型id
     * @return 结果
     */
    @ApiOperation(value = "删除字典类型")
    @ApiImplicitParam(name = "dictId", value = "字典类型id", required = true)
    @Log(title = "字典模块", businessType = "3", description = "删除字典类型")
    @PreAuthorize("hasRole('system:dict:deletedicttypebyid')")
    @DeleteMapping("/deleteDictTypeById/{dictId}")
    public AjaxResult deleteDictTypeById(@PathVariable Long dictId) {

        try {
            if (lyDictTypeService.deleteDictTypeById(dictId) > 0) {
                return AjaxResult.success("删除成功");
            }
        } catch (DictHaveDataException e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.error("删除失败");

    }

    /**
     * 批量删除字典类型
     *
     * @param ids 字典类型id（list）
     * @return 结果
     */
    @ApiOperation(value = "批量删除字典类型")
    @ApiImplicitParam(name = "ids", value = "字典类型id（list）", required = true)
    @Log(title = "字典模块", businessType = "3", description = "批量删除字典类型")
    @PreAuthorize("hasRole('system:dict:deletedicttypebyids')")
    @DeleteMapping("/deleteDictTypeByIds")
    public AjaxResult deleteDictTypeByIds(@RequestBody List<Long> ids) {
        try {
            if (lyDictTypeService.deleteDictTypeByIds(ids)) {
                return AjaxResult.success("删除成功");
            }
        } catch (DictHaveDataException e) {
            return AjaxResult.error("选中的部分" + e.getMessage());
        }
        return AjaxResult.error("删除失败");
    }

    /**
     * 获取字典类型名称(下拉框使用)
     *
     * @return 结果
     */
    @ApiOperation(value = "获取字典类型名称(下拉框使用)")
    @PreAuthorize("hasRole('system:dict:selectdicttypeall')")
    @GetMapping("/selectDictTypeAll")
    public AjaxResult selectDictTypeAll() {
        return AjaxResult.success("查询成功", lyDictTypeService.selectDictTypeAll());
    }


    /*
     *
     * ----------------------------------------------------------------------------------------------------------------------------------------------------
     */


    /**
     * 根据字典类型查询字典数据（全局使用vue）
     *
     * @param dictType 字典类型
     * @return 结果
     */
    @ApiOperation(value = "根据字典类型查询字典数据（全局使用vue）")
    @ApiImplicitParam(name = "dictType", value = "字典类型", required = true)
    @GetMapping("/queryDictDataByType/{dictType}")
    public AjaxResult queryDictDataByType(@PathVariable("dictType") String dictType) {
        return AjaxResult.success(lyDictDataService.queryDictDataByType(dictType));
    }

    /**
     * 根据条件分页查询字典数据列表
     *
     * @param dictDataPage 字典数据筛选条件
     * @return 结果
     */
    @ApiOperation(value = "根据条件分页查询字典数据列表")
    @Log(title = "字典模块", businessType = "0", description = "根据条件分页查询字典数据列表")
    @PreAuthorize("hasRole('system:dict:dictdatalist')")
    @GetMapping("/dictDataList")
    public AjaxResult dictDataList(DictDataPage dictDataPage) {
        return AjaxResult.success("查询成功", lyDictDataService.dictDataList(dictDataPage));
    }

    /**
     * 添加字典数据
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    @ApiOperation(value = "添加字典数据")
    @Log(title = "字典模块", businessType = "1", description = "添加字典数据")
    @PreAuthorize("hasRole('system:dict:adddictdata')")
    @PostMapping("/addDictData")
    public AjaxResult addDictData(@RequestBody LyDictData dictData) {
        dictData.setCreateTime(new Date());
        dictData.setUpdateTime(new Date());
        if (lyDictDataService.save(dictData)) {
            return AjaxResult.success("添加成功");
        }
        return AjaxResult.success("添加失败");
    }


    /**
     * 根据字典数据编码查询字典数据
     *
     * @param dictCode 字典编码
     * @return 字典数据信息
     */
    @ApiOperation(value = "根据字典数据编码查询字典数据")
    @ApiImplicitParam(name = "dictCode", value = "字典编码", required = true)
    @PreAuthorize("hasRole('system:dict:selectDictdatabyid')")
    @GetMapping("/selectDictDataById/{dictCode}")
    public AjaxResult selectDictDataById(@PathVariable Long dictCode) {
        return AjaxResult.success(lyDictDataService.selectDictDataById(dictCode));
    }

    /**
     * 修改字典数据
     *
     * @param dictData 字典数据信息
     * @return 结果
     */
    @ApiOperation(value = "修改字典数据")
    @Log(title = "字典模块", businessType = "2", description = "修改字典数据")
    @PreAuthorize("hasRole('system:dict:updatedictdata')")
    @PutMapping("/updateDictData")
    public AjaxResult updateDictData(@RequestBody LyDictData dictData) {
        dictData.setUpdateTime(new Date());
        if (lyDictDataService.updateById(dictData)) {
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.success("修改失败");
    }


    /**
     * 删除字典数据
     *
     * @param dictCode 字典编码
     * @return 结果
     */
    @ApiOperation(value = "删除字典数据")
    @ApiImplicitParam(name = "dictCode", value = "字典编码", required = true)
    @Log(title = "字典模块", businessType = "3", description = "删除字典数据")
    @PreAuthorize("hasRole('system:dict:deletedictdatabyid')")
    @DeleteMapping("/deleteDictDataById/{dictCode}")
    public AjaxResult deleteDictDataById(@PathVariable Long dictCode) {
        if (lyDictDataService.removeById(dictCode)) {
            return AjaxResult.success("删除成功");
        }
        return AjaxResult.success("删除失败");
    }

    /**
     * 批量删除字典数据
     *
     * @param ids 字典编码（list）
     * @return 结果
     */
    @ApiOperation(value = "批量删除字典数据")
    @ApiImplicitParam(name = "ids", value = "字典编码（list）", required = true)
    @Log(title = "字典模块", businessType = "3", description = "批量删除字典数据")
    @PreAuthorize("hasRole('system:dict:deletedictdatabyids')")
    @DeleteMapping("/deleteDictDataByIds")
    public AjaxResult deleteDictDataByIds(@RequestBody List<Long> ids) {
        if (lyDictDataService.removeByIds(ids)) {
            return AjaxResult.success("删除成功");
        }
        return AjaxResult.success("删除失败");
    }


}
