import request from '@/utils/request'


/**
 * 根据文章id 获取内容
 *
 * @returns {*}
 */
export function queryArticle(id) {

    return request({
        url: '/web/es/queryArticle/'+id,
        method: 'get',
    })

}