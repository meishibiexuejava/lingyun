package com.lingyun.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.pojo.LyLog;
import com.lingyun.common.vo.log.LogSearch;

import java.util.List;


/**
 * 日志表(LyLog)表服务接口
 *
 * @author 没事别学JAVA
 * @since 2023-05-07 22:56:35
 */
public interface LyLogService extends IService<LyLog> {


    /**
     * 根据条件分页查询日志列表
     * @param logSearch 日志筛选条件
     * @return 结果
     */
    public IPage<LyLog> logList(LogSearch logSearch);



}
