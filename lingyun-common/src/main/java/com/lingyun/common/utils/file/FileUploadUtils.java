package com.lingyun.common.utils.file;

import com.lingyun.common.exception.FileNameLengthLimitExceededException;
import com.lingyun.common.exception.FileSizeLimitExceededException;
import com.lingyun.common.exception.InvalidExtensionException;
import com.lingyun.common.utils.DateUtils;
import com.lingyun.common.utils.StringUtils;
import com.lingyun.common.utils.md5.Md5Utils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * 文件上传工具类
 *
 * @author ruoyi
 */
public class FileUploadUtils {

    /**
     * 默认大小 50M
     */
    public static final long DEFAULT_MAX_SIZE = 50 * 1024 * 1024;

    /**
     * 默认的文件名最大长度 100
     */
    public static final int DEFAULT_FILE_NAME_LENGTH = 100;


    /**
     * 根据文件路径上传
     *
     * @param baseDir 相对应用的基目录
     * @param file    上传的文件
     * @return 文件名称
     * @throws IOException
     */
    public static final String upload(String baseDir, MultipartFile file) throws IOException, FileSizeLimitExceededException, InvalidExtensionException{
        return upload(baseDir, file, MimeTypeUtils.DEFAULT_ALLOWED_EXTENSION);
    }

    /**
     * 文件上传
     *
     * @param baseDir          相对应用的根目录
     * @param file             上传的文件
     * @param allowedExtension 上传文件类型
     * @return 返回上传成功的文件名
     * @throws FileSizeLimitExceededException       如果超出最大大小
     * @throws FileNameLengthLimitExceededException 文件名太长
     * @throws IOException                          比如读写文件出错时
     * @throws InvalidExtensionException            文件校验异常
     */
    public static final String upload(String baseDir, MultipartFile file, String[] allowedExtension)
            throws FileSizeLimitExceededException, IOException, FileNameLengthLimitExceededException, InvalidExtensionException {

        int fileNamelength = file.getOriginalFilename().length(); //获取图片名字长度
        if (fileNamelength > FileUploadUtils.DEFAULT_FILE_NAME_LENGTH) {
            throw new FileNameLengthLimitExceededException("文件名太长,默认的文件名最大长度 100");
        }
        assertAllowed(file, allowedExtension);
        String fileName = extractFilename(file);
        File desc = getAbsoluteFile(baseDir, fileName);  //创建文件
        file.transferTo(desc);  //将文件上传到磁盘中

        if(baseDir.indexOf(":")==1){
            return StringUtils.substring(baseDir,2) + File.separator + fileName;
        }
        return baseDir + File.separator + fileName;
    }


    /**
     * 文件大小校验
     *
     * @param file             上传的文件
     * @param allowedExtension 上传文件类型
     * @throws FileSizeLimitExceededException 如果超出最大大小
     * @throws InvalidExtensionException      文件校验异常
     */
    public static final void assertAllowed(MultipartFile file, String[] allowedExtension) throws FileSizeLimitExceededException, InvalidExtensionException {
        long size = file.getSize();
        if (DEFAULT_MAX_SIZE != -1 && size > DEFAULT_MAX_SIZE) {
            throw new FileSizeLimitExceededException("默认大小 50M");
        }
        String fileName = file.getOriginalFilename();
        String extension = getExtension(file);
        if (allowedExtension != null && !isAllowedExtension(extension, allowedExtension)) {
            if (allowedExtension == MimeTypeUtils.IMAGE_EXTENSION) {
                throw new InvalidExtensionException(allowedExtension + extension + fileName);
            } else if (allowedExtension == MimeTypeUtils.FLASH_EXTENSION) {
                throw new InvalidExtensionException(allowedExtension + extension + fileName);
            } else if (allowedExtension == MimeTypeUtils.MEDIA_EXTENSION) {
                throw new InvalidExtensionException(allowedExtension + extension + fileName);
            } else if (allowedExtension == MimeTypeUtils.VIDEO_EXTENSION) {
                throw new InvalidExtensionException(allowedExtension + extension + fileName);
            } else {
                throw new InvalidExtensionException(allowedExtension + extension + fileName);
            }

        }
    }

    /**
     * 编码文件名
     *
     * @param file 上传的文件
     * @return yyyy/MM/dd/加密后的名字/图片后缀
     */
    public static final String extractFilename(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String extension = getExtension(file);
        fileName = DateUtils.datePath() + "/" + Md5Utils.hash(fileName + System.nanoTime()) + "." + extension;
        return fileName;
    }


    /**
     * 获取文件名的后缀
     *
     * @param file 上传的文件
     * @return 后缀名
     */
    public static final String getExtension(MultipartFile file) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (StringUtils.isEmpty(extension)) {
            extension = MimeTypeUtils.getExtension(file.getContentType());
        }
        return extension;
    }


    /**
     * 判断MIME类型是否是允许的MIME类型
     *
     * @param extension        图片后缀名
     * @param allowedExtension 后缀名集合
     * @return 允许返回true  不允许返回false
     */
    public static final boolean isAllowedExtension(String extension, String[] allowedExtension) {
        for (String str : allowedExtension) {
            if (str.equalsIgnoreCase(extension)) {  //equalsIgnoreCase() 方法用于将字符串与指定的对象比较，不考虑大小写。
                return true;
            }
        }
        return false;
    }

    /**
     * @param uploadDir 图片上传根目录
     * @param fileName  yyyy/MM/dd/加密后的名字/图片后缀
     * @return 完整的路径 、Windows系统就是盘符加路径
     */
    private static final File getAbsoluteFile(String uploadDir, String fileName) throws IOException {
        File desc = new File(uploadDir + File.separator + fileName);
        if (!desc.exists()) {
            if (!desc.getParentFile().exists()) {
                desc.getParentFile().mkdirs();
            }
        }
        return desc;
    }


}
