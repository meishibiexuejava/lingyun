package com.lingyun.system.quartz;


import com.lingyun.common.pojo.LyJob;
import com.lingyun.common.utils.quartz.AbstractQuartzJob;
import com.lingyun.common.utils.quartz.JobInvokeUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 禁止并发执行
 */
@DisallowConcurrentExecution
public class QuartzDisallowConcurrentExecution extends AbstractQuartzJob {

    @Override
    protected void doExecute(JobExecutionContext context, LyJob job) throws Exception {
        JobInvokeUtil.invokeMethod(job);
    }
}


