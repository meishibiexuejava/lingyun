package com.lingyun.common.vo.log;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import java.io.Serializable;

/**
 * @author meishibiexuejava
 * 2023/5/7 23:04
 */
@Data
@ToString
@ApiModel(value = "LogSearch", description = "日志筛选条件")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LogSearch implements Serializable {


    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private Integer current;
    /**
     * 页大小
     */
    @ApiModelProperty(value = "页大小", required = true)
    private Integer size;

    /**
     *日志模块
     */
    @ApiModelProperty(value = "日志模块")
    private String logTitle;

    /**
     *日志业务类型（0查询 1 新增 2修改 3删除 4上传图片 5其他 6查看 7导出 8绑定 9取消绑定）
     */
    @ApiModelProperty(value = "日志业务类型（0查询 1 新增 2修改 3删除 4上传图片 5其他 6查看 7导出 8绑定 9取消绑定）")
    private String logBusinessType;

    /**
     *操作状态（1正常 2异常）
     */
    @ApiModelProperty(value = "操作状态（1正常 2异常）")
    private String logOperationStatus;



}
