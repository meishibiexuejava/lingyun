package com.lingyun.common.vo.quartz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * @author meishibiexuejava
 * 2023/5/2 0:02
 */
@Data
@ToString
@ApiModel(value = "QuartzStatus", description = "定时任务修改状态")
public class QuartzStatus {

    /**
     * 任务ID
     */
    @ApiModelProperty(value = "任务ID")
    private Long jobId;

    /**
     * 状态（0运行中 1暂停）
     */
    @ApiModelProperty(value = "状态（0运行中 1暂停）", example = "0")
    private String jobStatus;

}
