/**
 * 表单重置
 * @param refName
 */
export function resetForm(refName) {
    if (this.$refs[refName]) {
        this.$refs[refName].resetFields();
    }
}


/**
 * 构造树型结构数据
 * @param {*} data 数据源
 * @param {*} id id字段 默认 'id'
 * @param {*} parentId 父节点字段 默认 'parentId'
 * @param {*} children 孩子节点字段 默认 'children'
 */
export function handleTree(data, id, parentId, children) {
    let config = {
        id: id || 'id',
        parentId: parentId || 'parentId',
        childrenList: children || 'children'
    };

    const childrenListMap = {};
    const nodeIds = {};
    const tree = [];

    for (let d of data) {
        let parentId = d[config.parentId];
        if (childrenListMap[parentId] == null) {
            childrenListMap[parentId] = [];
        }
        nodeIds[d[config.id]] = d;
        childrenListMap[parentId].push(d);
    }

    for (let d of data) {
        let parentId = d[config.parentId];
        if (nodeIds[parentId] == null) {
            tree.push(d);
        }
    }

    for (let t of tree) {
        adaptToChildrenList(t);
    }

    function adaptToChildrenList(o) {
        if (childrenListMap[o[config.id]] !== null) {
            o[config.childrenList] = childrenListMap[o[config.id]];
        }
        if (o[config.childrenList]) {
            for (let c of o[config.childrenList]) {
                adaptToChildrenList(c);
            }
        }
    }

    return tree;
}


/**
 * 配置图片加速
 */
export function lingyun_img(url) {
    return process.env.VUE_APP_IMAGE_DOMAIN + process.env.VUE_APP_BASE_API + url
}

/**
 * 配置视频加速
 */
export function lingyun_video(url) {
    return process.env.VUE_APP_VIDEO_DOMAIN + process.env.VUE_APP_BASE_API + url
}

/**
 * 配置静态资源加速（js）
 */
export function lingyun_cdn(url) {
    return process.env.VUE_APP_CDN_DOMAIN + process.env.VUE_APP_BASE_API + url
}
