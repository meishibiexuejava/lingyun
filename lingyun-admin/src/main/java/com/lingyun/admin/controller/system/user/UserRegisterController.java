package com.lingyun.admin.controller.system.user;

import com.lingyun.common.annotation.Log;
import com.lingyun.common.exception.LyException;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.system.service.LyUserService;
import com.lingyun.common.vo.UserRegister;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;


/**
 * 后台注册
 *
 * @author 没事别学JAVA
 */
@RestController
@RequestMapping("/system/register")
@Api(value = "后台注册", tags = "后台注册")
public class UserRegisterController {

    @Resource
    private LyUserService lyUserService;

    /**
     * 邮箱注册-发送验证码
     */
    @ApiOperation(value = "邮箱注册-发送验证码")
    @PostMapping("/sendemailverificationcode")
    public AjaxResult SendEmailVerificationCode(@RequestParam("Email") String Email) {
        try {
            lyUserService.SendEmailVerificationCode(Email);
        } catch (LyException e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success("发送成功");
    }

    /**
     * 后台注册
     * @param userRegister 用户注册表单
     * @return 结果
     */
    @ApiOperation(value = "后台注册")
    @Log(title = "登录模块", businessType = "11", description = "后台注册")
    @PostMapping
    public AjaxResult register(@RequestBody UserRegister userRegister) {
        try {
            lyUserService.register(userRegister);
        } catch (LyException e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success("注册成功");
    }




}
