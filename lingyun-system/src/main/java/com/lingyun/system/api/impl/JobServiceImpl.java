package com.lingyun.system.api.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lingyun.common.corebase.GeneralEntity;
import com.lingyun.common.mapper.JobMapper;
import com.lingyun.common.pojo.JobFrom;
import com.lingyun.common.pojo.JobAndTrigger;
import com.lingyun.system.api.JobService;
import org.quartz.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 没事别学JAVA
 * 2021/4/27 18:03
 */
@Service("JobService")
public class JobServiceImpl implements JobService {

    @Resource
    private Scheduler scheduler;

    @Resource
    private JobMapper jobmapper;

    @Override
    public IPage<JobAndTrigger> quartzList(GeneralEntity generalEntity) {
        Page<JobAndTrigger> page = new Page<>(generalEntity.getCurrent(), generalEntity.getSize());
        IPage<JobAndTrigger> iPage = jobmapper.quartzList(page);
        return iPage;
    }

    @Override
    public void quartzAdd(JobFrom job) throws SchedulerException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        Class<? extends Job> jobClass = (Class<? extends Job>) (Class.forName(job.getJobClassName()).newInstance().getClass());
        // 启动调度器
        scheduler.start();
        // 构建Job信息
        JobDetail jobDetail = JobBuilder
                .newJob(jobClass)
                .withIdentity(JobKey.jobKey(job.getJobClassName(), job.getJobGroupName()))
                .build();

        // Cron表达式调度构建器(即任务执行的时间)
        CronScheduleBuilder cron = CronScheduleBuilder.cronSchedule(job.getCronExpression());

        //根据Cron表达式构建一个Trigger
        CronTrigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity(TriggerKey.triggerKey(job.getJobClassName(), job.getJobGroupName()))
                .withSchedule(cron)
                .build();

        // trigger.getJobDataMap().put("lyArticle", job);

        // 是否存在
        if(scheduler.checkExists(JobKey.jobKey(job.getJobClassName(), job.getJobGroupName()))){
            // 防止数据库中的数据被修改 先移除，然后在执行创建操作
            scheduler.deleteJob(JobKey.jobKey(job.getJobClassName(), job.getJobGroupName()));
        }

        // 执行调度任务
        scheduler.scheduleJob(jobDetail, trigger);

//        // 暂停
//        scheduler.pauseJob(JobKey.jobKey(job.getJobClassName(), job.getJobGroupName()));


//        try {
//            scheduler.scheduleJob(jobDetail, trigger);
//        } catch (SchedulerException e) {
//            System.out.println(e.getMessage());
//            throw new SchedulerException("【定时任务】创建失败！");
//        }

    }

    @Override
    public void deleteJob(JobFrom job) throws Exception {
        scheduler.pauseTrigger(TriggerKey.triggerKey(job.getJobClassName(), job.getJobGroupName()));
        scheduler.unscheduleJob(TriggerKey.triggerKey(job.getJobClassName(), job.getJobGroupName()));
        scheduler.deleteJob(JobKey.jobKey(job.getJobClassName(), job.getJobGroupName()));
    }


}
