package com.lingyun.common.exception.quartz;

import com.lingyun.common.exception.LyException;

public class QuartzException extends LyException {

    private static final long serialVersionUID = 1L;

    public QuartzException(String message) {
        super("quartz", message, null);
    }


}
