package com.lingyun.common.exception.user;

import com.lingyun.common.utils.MessageUtils;

/**
 * 用户认证异常
 *  1、用户不存在/密码错误
 *  2、该用户未拥有角色，无法登录
 *
 *
 *
 * @author 没事别学JAVA
 * 2021/12/25 2:16
 */
public class UserAuthenticationException extends UserException{

    private static final long serialVersionUID = 1L;

    public UserAuthenticationException(String message) {
        super(MessageUtils.message(message));
    }
}
