package com.lingyun.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lingyun.common.mapper.LyConfigMapper;
import com.lingyun.common.pojo.LyConfig;
import com.lingyun.system.service.LyConfigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (LyConfig)表服务实现类
 *
 * @author 没事别学JAVA
 * @since 2024-04-23 18:33:16
 */
@Service("lyConfigService")
public class LyConfigServiceImpl extends ServiceImpl<LyConfigMapper, LyConfig> implements LyConfigService {


    @Resource
    LyConfigMapper lyricConfigMapper;



}
