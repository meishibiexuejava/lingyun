package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.LyConfig;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

/**
 * (LyConfig)
 *
 * @author 没事别学JAVA
 * @since 2024-04-23 18:30:12
 */
public interface LyConfigMapper extends BaseMapper<LyConfig> {

    /**
     * 全部查询
     *
     * @return 对象列表
     */
    public List<LyConfig> queryAll();


    /**
     * 根据条件分页查询文章列表
     * @param page 分页
     * @param queryWrapper 条件
     * @return 文章列表
     */
     public IPage<LyConfig> lyConfigList(IPage<LyConfig> page, @Param("ew") Wrapper<LyConfig> queryWrapper);



    /**
     * 单个查询
     *
     * @param configId 主键
     * @return 对象列表
     */
     public LyConfig queryById(Long configId);


    /**
     * 新增
     *
     * @param lyConfig
     * @return 结果
     */
    public Integer insertLyConfig(LyConfig lyConfig);


    /**
     * 选择性插入
     *
     * @param lyConfig
     * @return 结果
     */
    public Integer insertSelective(LyConfig lyConfig);


    /**
     * 单个删除
     *
     * @param configId 主键
     * @return 结果
     */
    public Integer deleteById(Long configId);


    /**
     * 通过主键修改数据
     *
     * @param lyConfig
     * @return 结果
     */
    public Integer updateLyConfig(LyConfig lyConfig);


    /**
     * 通过主键选择性修改
     *
     * @param lyConfig
     * @return 结果
     */
    public Integer updateSelective(LyConfig lyConfig);

}
