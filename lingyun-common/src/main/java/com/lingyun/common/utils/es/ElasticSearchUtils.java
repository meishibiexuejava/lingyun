package com.lingyun.common.utils.es;

import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

/**
 *
 * elasticsearch 工具类
 *
 */
@Component
public class ElasticSearchUtils {

    @Resource
    RestHighLevelClient restHighLevelClient;



    /**
     * 判断一个索引是否存在
     * @param indexName  索引名称
     * @return true存在   false没有
     */
    public boolean isExistsIndex(String indexName){
        GetIndexRequest request = new GetIndexRequest(indexName);
        try {
            return restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     *判断一个文档是否存在
     * @param indexName   索引名称
     * @param id          文档id
     * @return true存在   false没有
     */
    public boolean isExistsDocument(String indexName, String id){
        GetRequest getRequest = new GetRequest(indexName,id);
        boolean exists = false;
        try {
            exists = restHighLevelClient.exists(getRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return exists;
    }






}
