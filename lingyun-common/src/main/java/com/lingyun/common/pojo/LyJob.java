package com.lingyun.common.pojo;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 定时任务调度表(LyJob)表实体类
 *
 * @author 没事别学JAVA
 * @since 2023-01-02 21:35:00
 */
@Data
@ToString
@TableName(value = "ly_job")
@ApiModel(value = "LyJob", description = "定时任务信息")
public class LyJob implements Serializable {
    /**
     * 任务ID
     */
    @TableId(value = "job_id", type = IdType.AUTO)
    @ApiModelProperty(value = "任务ID", example = "null")
    private Long jobId;
    /**
     * 任务名称
     */
    @ApiModelProperty(value = "任务名称", required = true, example = "测试001")
    private String jobName;
    /**
     * 任务组名
     */
    @ApiModelProperty(value = "任务组名", required = true, example = "DEFAULT")
    private String jobGroup;
    /**
     * 调用目标字符串
     */
    @ApiModelProperty(value = "调用目标字符串", required = true, example = "lyTask.lyParams('ly')")
    private String invokeTarget;
    /**
     * cron执行表达式
     */
    @ApiModelProperty(value = "cron执行表达式", required = true, example = "0/2 * * * * ?")
    private String cronExpression;
    /**
     * 计划执行错误策略（1立即执行 2执行一次 3放弃执行 4执行一次删除）
     */
    @ApiModelProperty(value = "计划执行错误策略（1立即执行 2执行一次 3放弃执行 4执行一次删除）", example = "1")
    private String misfirePolicy;
    /**
     * 是否并发执行（0允许 1禁止）
     */
    @ApiModelProperty(value = "是否并发执行（0允许 1禁止）", example = "0")
    private String concurrent;
    /**
     * 状态（0运行中 1暂停）
     */
    @ApiModelProperty(value = "状态（0运行中 1暂停）", example = "0")
    private String jobStatus;
    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者", hidden = true)
    private String createBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", hidden = true)
    private Date createTime;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者", hidden = true)
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", hidden = true)
    private Date updateTime;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", example = "备注测试001")
    private String remark;


    public LyJob() {
    }

    public LyJob(Long jobId, String jobStatus) {
        this.jobId = jobId;
        this.jobStatus = jobStatus;
    }


}
