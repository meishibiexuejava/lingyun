import request from '@/utils/request'



/**
 * 注册
 * @param data
 * @return {*}
 */
export function register(data){

    return request({
        url: '/system/register',
        method: 'post',
        data: data
    })

}
