package com.lingyun.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.corebase.GeneralEntity;
import com.lingyun.common.exception.role.RoleHaveUserCanNotDelException;
import com.lingyun.common.pojo.LyRole;
import com.lingyun.common.pojo.LyUser;
import com.lingyun.common.pojo.LyUserRole;
import com.lingyun.common.vo.role.AllocationUserCondition;
import com.lingyun.common.vo.role.RoleVagueQuery;
import com.lingyun.common.vo.user.UserQueryCriteria;

import java.util.List;

/**
 * @author 没事别学JAVA
 * 2021/12/16 0:56
 */
public interface LyRoleService extends IService<LyRole> {


    /**
     * 查询角色列表（下拉框）
     *
     * @return 角色列表
     */
    public List<LyRole> selectOption();

    /**
     * 根据条件分页查询角色列表
     *
     * @param generalEntity 角色搜索表单
     * @return 角色分页数据
     */
    public IPage<LyRole> roleList(GeneralEntity generalEntity);

    /**
     * 添加角色
     *
     * @param lyRole 角色信息
     * @return 结果
     */
    public Integer addRole(LyRole lyRole);

    /**
     * 根据角色ID查询角色信息
     *
     * @param roleId 角色ID
     * @return 角色信息
     */
    public LyRole selectRoleById(Long roleId);

    /**
     * 修改角色
     *
     * @param lyRole 角色信息
     * @return 结果
     */
    public Integer updateRole(LyRole lyRole);

    /**
     * 删除角色
     *
     * @param roleId 角色id
     * @return 结果
     */
    public boolean deleteRoleById(Long roleId) throws RoleHaveUserCanNotDelException;


    /**
     * 导出角色信息(Excel)
     *
     * @param generalEntity 角色筛选条件
     * @return 文件名字
     */
    public List<LyRole> roleExportExcel(GeneralEntity generalEntity);

    /**
     * 根据角色id分页查询用户
     *
     * @param allocationUserCondition 分配用户搜索条件
     * @return 结果
     */
    public IPage<LyUser> selectRoleUser(AllocationUserCondition allocationUserCondition);

    /**
     * 取消绑定
     *
     * @param lyUserRole 角色id、用户id
     * @return 结果
     */
    public Boolean cancelBinding(LyUserRole lyUserRole);

    /**
     * 批量取消绑定
     *
     * @param roleId  角色id
     * @param userIds 用户id
     * @return 结果
     */
    public Boolean batchCancelBinding(Long roleId, Long[] userIds);

    /**
     * 绑定用户
     *
     * @param roleId  角色id
     * @param userIds 用户id
     * @return 结果
     */
    public Boolean bindingUser(Long roleId, Long[] userIds);




}
