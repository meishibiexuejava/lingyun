package com.lingyun.admin.controller.system.user;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.lingyun.common.annotation.Log;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.utils.file.FileDownloadUtils;
import com.lingyun.common.vo.role.AllocationUserCondition;
import com.lingyun.common.vo.user.UserAll;
import com.lingyun.common.vo.user.UserQueryCriteria;
import com.lingyun.common.vo.user.UserFrom;
import com.lingyun.system.service.LyUserService;
import io.swagger.annotations.*;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * 用户管理
 *
 * @author 没事别学JAVA
 */
@RestController
@RequestMapping("/system/user")
@Api(value = "用户管理", tags = "用户管理")
public class UserController {

    @Resource
    LyUserService lyUserService;

    /**
     * 根据条件分页查询用户列表
     *
     * @param userQueryCriteria 用户筛选条件
     * @return 用户列表
     */
    @ApiOperation(value = "根据条件分页查询用户列表")
    @Log(title = "用户模块", businessType = "0", description = "根据条件分页查询用户列表")
    @PreAuthorize("hasRole('system:user:getuserlist')")
    @GetMapping("/getUserList")
    public AjaxResult getUserList(UserQueryCriteria userQueryCriteria) {
        return AjaxResult.success("查询成功", lyUserService.getUserList(userQueryCriteria));
    }


    /**
     * 添加用户
     *
     * @param userFrom 用户信息
     * @return 结果
     */
    @ApiOperation(value = "添加用户")
    @Log(title = "用户模块", businessType = "1", description = "添加用户")
    @PreAuthorize("hasRole('system:user:adduser')")
    @PostMapping("/addUser")
    public AjaxResult addUser(@RequestBody UserFrom userFrom) {
        try {
            lyUserService.addUser(userFrom);
        } catch (Exception e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success("添加成功");
    }

    /**
     * 根据id查询用户详细信息
     *
     * @param userId 用户id
     * @return 结果
     */
    @ApiOperation(value = "根据id查询用户详细信息")
    @PreAuthorize("hasRole('system:user:selectuserbyid')")
    @GetMapping("/selectUserById/{userId}")
    public AjaxResult selectUserById(@ApiParam(name = "userId", value = "用户id", required = true) @PathVariable("userId") Long userId) {
        AjaxResult ajax = AjaxResult.success();
        // 根据id查询用户详细信息
        ajax.put("data", lyUserService.selectUserById(userId));
        // 根据用户id 获取角色列表
        ajax.put("roleIds", lyUserService.getUserRole(userId));
        return ajax;
    }

    /**
     * 修改用户
     *
     * @param userFrom 用户信息
     * @return 结果
     */
    @ApiOperation(value = "修改用户")
    @Log(title = "用户模块", businessType = "2", description = "修改用户")
    @PreAuthorize("hasRole('system:user:updateuser')")
    @PutMapping("/updateUser")
    public AjaxResult updateUser(@RequestBody UserFrom userFrom) {
        if (lyUserService.updateUser(userFrom)) {
            return AjaxResult.success("修改成功");
        } else {
            return AjaxResult.error("修改失败");
        }
    }

    /**
     * 删除用户
     *
     * @param userId 用户id
     * @return 结果
     */
    @ApiOperation(value = "删除用户")
    @Log(title = "用户模块", businessType = "3", description = "删除用户")
    @PreAuthorize("hasRole('system:user:deletuserbyid')")
    @DeleteMapping("/deletUserById/{userId}")
    public AjaxResult deletUserById(@ApiParam(name = "userId", value = "用户id", required = true) @PathVariable("userId") Long userId) {
        if (lyUserService.removeById(userId)) {
            return AjaxResult.success("删除成功");
        } else {
            return AjaxResult.error("删除成功");
        }
    }

    /**
     * 批量删除用户
     *
     * @param userIds 用户id(list)
     * @return 结果
     */
    @ApiOperation(value = "批量删除用户")
    @ApiImplicitParam(name = "userIds", value = "用户id(list)", required = true)
    @Log(title = "用户模块", businessType = "3", description = "批量删除用户")
    @PreAuthorize("hasRole('system:user:deletuserbyids')")
    @DeleteMapping("/deletUserByIds")
    public AjaxResult deletUserByIds(@RequestBody List<Long> userIds) {
        if (lyUserService.removeByIds(userIds)) {
            return AjaxResult.success("删除成功");
        } else {
            return AjaxResult.error("删除成功");
        }
    }


    /**
     * 导出用户信息(Excel)
     *
     * @param userQueryCriteria 用户筛选条件
     * @return 文件名字
     */
    @ApiOperation(value = "导出用户信息(Excel)")
    @Log(title = "用户模块", businessType = "7", description = "导出用户信息(Excel)")
    @PreAuthorize("hasRole('system:user:userexportexcel')")
    @GetMapping("/userExportExcel")
    public AjaxResult userExportExcel(UserQueryCriteria userQueryCriteria) {
        List<UserAll> list = lyUserService.userExportExcel(userQueryCriteria);
        Workbook work = ExcelExportUtil.exportExcel(new ExportParams(), UserAll.class, list);
        String filename = UUID.randomUUID().toString() + "_" + "用户数据" + ".xlsx";
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(FileDownloadUtils.getAbsoluteFile(filename));
            work.write(out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return AjaxResult.success("导出成功", filename);
    }

    /**
     * 获取授权用户列表
     *
     * @param allocationUserCondition 分配用户搜索条件
     * @return 结果
     */
    @ApiOperation(value = "授权用户列表")
    @Log(title = "用户模块", businessType = "0", description = "获取授权用户列表")
    @PreAuthorize("hasRole('system:user:authorizationuserlist')")
    @GetMapping("/authorizationUserList")
    public AjaxResult authorizationUserList(AllocationUserCondition allocationUserCondition) {
        return AjaxResult.success(lyUserService.authorizationUserList(allocationUserCondition));
    }


}
