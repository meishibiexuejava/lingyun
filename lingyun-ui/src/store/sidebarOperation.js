import Cookies from 'js-cookie'

const sidebarOperation ={

    state: {
        sidebar: {
            /**
             * Cookies.get(‘sidebarStatus’))的意思是取出来的是 "0"
             * +Cookies.get(‘sidebarStatus’)的意思是 +"0" , 触发隐式类型转换, 会得到 0
             * !+Cookies.get(‘sidebarStatus’))的意思是 !0 , 对数值类型取反会返回布尔类型, 会得到 true
             * !!+Cookies.get(‘sidebarStatus’)的意思是 !true , 对布尔的true进行取反, 会得到 false
             *
             * 由于cookie取出的值都是string类型, 但是代码中需要用的是布尔类型,
             * !!+会把字符串的 “0”, 转成 false,
             * 或者把 字符串的 “1”, 转成 true
             */
            opened: Cookies.get('sidebarStatus') ? !!+Cookies.get('sidebarStatus') : true,
            withoutAnimation: false,
            hide: false
        },
        device: 'desktop',
        size: Cookies.get('size') || 'medium'
    },
    mutations: {
        TOGGLE_SIDEBAR: state => {
            if (state.sidebar.hide) {
                return false;
            }
            state.sidebar.opened = !state.sidebar.opened
            state.sidebar.withoutAnimation = false
            if (state.sidebar.opened) {
                Cookies.set('sidebarStatus', 1)
            } else {
                Cookies.set('sidebarStatus', 0)
            }
        },
        CLOSE_SIDEBAR: (state, withoutAnimation) => {
            Cookies.set('sidebarStatus', 0)
            state.sidebar.opened = false
            state.sidebar.withoutAnimation = withoutAnimation
        },
        TOGGLE_DEVICE: (state, device) => {
            state.device = device
        },
        SET_SIZE: (state, size) => {
            state.size = size
            Cookies.set('size', size)
        },
        SET_SIDEBAR_HIDE: (state, status) => {
            state.sidebar.hide = status
        }
    },
    actions: {
        toggleSideBar({ commit }) {
            commit('TOGGLE_SIDEBAR')
        },
        closeSideBar({ commit }, { withoutAnimation }) {
            commit('CLOSE_SIDEBAR', withoutAnimation)
        },
        toggleDevice({ commit }, device) {
            commit('TOGGLE_DEVICE', device)
        },
        setSize({ commit }, size) {
            commit('SET_SIZE', size)
        },
        toggleSideBarHide({ commit }, status) {
            commit('SET_SIDEBAR_HIDE', status)
        }
    }


}

export default sidebarOperation;
