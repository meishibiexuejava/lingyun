package com.lingyun.common.exception.dict;

import com.lingyun.common.utils.MessageUtils;


/**
 * 字典类型拥有数据，无法删除
 */
public class DictHaveDataException extends DictException {

    private static final long serialVersionUID = 1L;

    public DictHaveDataException() {
        super(MessageUtils.message("dict.have.data"));
    }

}
