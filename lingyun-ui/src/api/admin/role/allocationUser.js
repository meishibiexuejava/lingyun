import request from '@/utils/request'

/**
 * 根据角色id分页查询用户
 * @param query
 * @returns {*}
 */
export function selectRoleUser(query){

    return request({
        url: '/system/role/selectRoleUser',
        method: 'get',
        params: query
    })

}

/**
 * 取消绑定
 * @param roleId 角色idWW
 * @param userId 用户id
 * @returns {*}
 */
export function cancelBinding(roleId,userId){

    const data = {
        roleId,
        userId
    }
    return request({
        url: '/system/role/cancelBinding',
        method: 'put',
        data: data
    })

}

/**
 * 批量取消绑定
 * @param query 角色id 和 用户id
 * @returns {*}
 */
export function batchCancelBinding(query){

    return request({
        url: '/system/role/batchCancelBinding',
        method: 'put',
        params: query
    })

}


/**
 * 获取授权用户列表
 * @param query
 * @returns {*}
 */
export function authorizationUserList(query){

    return request({
        url: '/system/user/authorizationUserList',
        method: 'get',
        params: query
    })

}

/**
 * 绑定用户
 * @param query 角色id 和 用户id
 * @returns {*}
 */
export function bindingUser(query){

    return request({
        url: '/system/role/bindingUser',
        method: 'post',
        params: query
    })

}

