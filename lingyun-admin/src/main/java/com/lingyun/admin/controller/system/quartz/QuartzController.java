package com.lingyun.admin.controller.system.quartz;

import com.lingyun.common.annotation.Log;
import com.lingyun.common.exception.quartz.QuartzInOperationException;
import com.lingyun.common.pojo.LyJob;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.quartz.QuartzSearch;
import com.lingyun.common.vo.quartz.QuartzStatus;
import com.lingyun.system.api.JobService;
import com.lingyun.system.service.LyJobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.quartz.SchedulerException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 定时任务管理
 *
 * @author 没事别学JAVA
 */
@RestController
@RequestMapping("/system/quartz")
@Api(value = "定时任务管理", tags = "定时任务管理")
public class QuartzController {

    @Resource
    private JobService jobService;

    @Resource
    private LyJobService lyJobService;


    /**
     * 根据条件分页查询定时任务列表
     *
     * @param quartzSearch 定时任务筛选条件
     * @return 结果
     */
    @ApiOperation(value = "获取定时任务列表")
    @Log(title = "定时任务模块", businessType = "0", description = "根据条件分页查询定时任务列表")
    @PreAuthorize("hasRole('system:quartz:quartzlist')")
    @GetMapping("/quartzList")
    public AjaxResult quartzList(QuartzSearch quartzSearch) {
        return AjaxResult.success(lyJobService.quartzList(quartzSearch));
    }


    /**
     * 添加定时任务
     *
     * @param job 定时任务信息
     * @return 结果
     */
    @ApiOperation(value = "添加定时任务")
    @Log(title = "定时任务模块", businessType = "1", description = "添加定时任务")
    @PreAuthorize("hasRole('system:quartz:quartzadd')")
    @PostMapping("/quartzAdd")
    public AjaxResult quartzAdd(@RequestBody LyJob job) {

        try {
            lyJobService.quartzAdd(job);
        } catch (SchedulerException e) {
            AjaxResult.error(e.getMessage());
        }

        return AjaxResult.success("添加成功");
    }

    /**
     * 根据id查询定时任务
     *
     * @param jobId 定时任务id
     * @return 结果
     */
    @ApiOperation(value = "根据id查询定时任务")
    @ApiImplicitParam(name = "jobId", value = "定时任务id", required = true)
    @PreAuthorize("hasRole('system:quartz:getquartzbyid')")
    @GetMapping("/getQuartzById/{jobId}")
    public AjaxResult getQuartzById(@PathVariable Long jobId) {
        return AjaxResult.success(lyJobService.getById(jobId));
    }

    /**
     * 修改定时任务
     *
     * @param job 定时任务信息
     * @return 结果
     */
    @ApiOperation(value = "修改定时任务")
    @Log(title = "定时任务模块", businessType = "2", description = "修改定时任务")
    @PreAuthorize("hasRole('system:quartz:quartzupdate')")
    @PutMapping("/quartzUpdate")
    public AjaxResult quartzUpdate(@RequestBody LyJob job) {
        if (lyJobService.quartzUpdate(job) > 0) {
            return AjaxResult.success("修改成功");
        } else {
            return AjaxResult.error("修改失败");
        }
    }

    /**
     * 删除定时任务
     *
     * @param jobId 定时任务id
     * @return 结果
     */
    @ApiOperation(value = "删除定时任务")
    @ApiImplicitParam(name = "jobId", value = "定时任务id", required = true)
    @Log(title = "定时任务模块", businessType = "3", description = "删除定时任务")
    @PreAuthorize("hasRole('system:quartz:quartzdelete')")
    @DeleteMapping("/quartzDelete/{jobId}")
    public AjaxResult quartzDelete(@PathVariable Long jobId) {

        try {
            if (lyJobService.quartzDelete(jobId) > 0) {
                return AjaxResult.success("删除成功");
            }
        } catch (SchedulerException | QuartzInOperationException e) {
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success("删除失败");
    }


    /**
     * 批量删除定时任务
     *
     * @param jobIds 定时任务id(list)
     * @return 结果
     */
    @ApiOperation(value = "批量删除定时任务")
    @ApiImplicitParam(name = "jobIds", value = "定时任务id(list)", required = true)
    @Log(title = "定时任务模块", businessType = "3", description = "批量删除定时任务")
    @PreAuthorize("hasRole('system:quartz:quartzids')")
    @DeleteMapping("/quartzIds")
    public AjaxResult quartzIds(@RequestBody List<Long> jobIds) {
        if (lyJobService.removeByIds(jobIds)) {
            return AjaxResult.success("删除成功");
        } else {
            return AjaxResult.success("删除失败");
        }
    }


    /**
     * 修改定时任务状态
     *
     * @param quartzStatus 定时任务信息
     * @return 结果
     */
    @ApiOperation(value = "修改定时任务状态")
    @Log(title = "定时任务模块", businessType = "2", description = "修改定时任务状态")
    @PreAuthorize("hasRole('system:quartz:updatejobstatus')")
    @PutMapping("/updateJobStatus")
    public AjaxResult updateJobStatus(@RequestBody QuartzStatus quartzStatus) {
        if (lyJobService.updateJobStatus(quartzStatus) > 0) {
            return AjaxResult.success("修改成功");
        } else {
            return AjaxResult.error("修改失败");
        }

    }


}
