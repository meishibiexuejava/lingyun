package com.lingyun.common.vo.role;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lingyun.common.corebase.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 角色管理搜索表单
 *
 * @author 没事别学JAVA
 */
@Data
@ToString
@ApiModel(value = "角色筛选条件", description = "角色筛选条件")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RoleVagueQuery extends BaseEntity {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private Integer current;
    /**
     * 页大小
     */
    @ApiModelProperty(value = "页大小", required = true)
    private Integer size;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String roleName;
    /**
     * 权限字符
     */
    @ApiModelProperty(value = "权限字符")
    private String roleKey;

    /**
     * 状态
     */
    @ApiModelProperty(value = "角色状态（0启用 1停用）")
    private String roleStatus;


}
