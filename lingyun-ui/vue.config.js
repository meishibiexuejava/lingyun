const path = require('path')

function resolve(dir) {
    return path.join(__dirname, dir)
}

const name = process.env.VUE_APP_TITLE || '凌云管理系统' // 网页标题

const port = process.env.port || process.env.npm_config_port || 80 // 端口

module.exports = {

    // 基本路径
    publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
    // 在npm run build 或 yarn build 时 ，生成文件的目录名称（要和baseUrl的生产环境路径一致）（默认dist）
    outputDir: 'dist',
    // 用于放置生成的静态资源 (js、css、img、fonts) 的；（项目打包之后，静态资源会放在这个文件夹下）
    assetsDir: 'static',
    // 是否开启eslint保存检测，有效值：ture | false | 'error'
    lintOnSave: process.env.NODE_ENV === 'development',
    // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
    productionSourceMap: false,
    // webpack-dev-server 相关配置
    devServer: {
        host: '0.0.0.0',
        port: port,
        open: true,
        proxy: {
            // detail: https://cli.vuejs.org/config/#devserver-proxy'
            [process.env.VUE_APP_BASE_API]: {
                target: `http://localhost:8081`,
                changeOrigin: true,
                pathRewrite: {
                    ['^' + process.env.VUE_APP_BASE_API]: ''
                }
            }
        },
        //https:true,
        disableHostCheck: true
    },
    configureWebpack: {
        name: name,
        resolve: {
            alias: {
                '@': resolve('src')
            }
        }
    },


    /**
     *
     * require属性，用来引入插件。
     * path属性:输出的绝对路径
     * test属性，用于标识出应该被对应的 loader 进行转换的某个或某些文件(配置匹配正则规则)。
     * use属性，表示进行转换时，应该使用哪个 loader。
     * include属性：只处理指定的目录文件。
     * exclude属性：指定某目录文件不被处理。
     * options属性：当前loader需要的特殊配置(可选)。
     * resolve属性：设置模块如何被解析。
     *
     * */
    chainWebpack(config) {

        // set svg-sprite-loader
        config.module
            .rule('svg') //找个配置rule规则里面的svg
            .exclude.add(resolve('src/icons'))  //排除src/icons下的.svg文件
            .end()
        config.module
            .rule('icons')      //配置rule规则里面新增的icons规则
            .test(/\.svg$/)     //icons规则里匹配到.svg结尾的文件
            .include.add(resolve('src/icons'))   //包含src/icons下的.svg文件
            .end()
            .use('svg-sprite-loader')         //使用svg-sprite-loader
            .loader('svg-sprite-loader')
            .options({
                symbolId: 'icon-[name]'               //class名
            })
            .end()
    }


}
