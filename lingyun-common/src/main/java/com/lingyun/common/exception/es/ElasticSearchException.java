package com.lingyun.common.exception.es;

import com.lingyun.common.exception.LyException;

public class ElasticSearchException extends LyException {

    private static final long serialVersionUID = 1L;

    public ElasticSearchException(String message) {
        super("elasticsearch", message, null);
    }
}
