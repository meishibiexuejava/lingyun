package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lingyun.common.pojo.LyLog;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * 日志表(LyLog)
 *
 * @author 没事别学JAVA
 * @since 2023-03-28 23:37:36
 */
public interface LyLogMapper extends BaseMapper<LyLog> {

    /**
     * 全部查询
     *
     * @return 对象列表
     */
    public List<LyLog> queryAll();

    /**
     * 根据条件分页查询文章列表
     * @param page 分页
     * @param queryWrapper 条件
     * @return 文章列表
     */
    public IPage<LyLog> lyLogList(IPage<LyLog> page, @Param("ew") Wrapper<LyLog> queryWrapper);

    /**
     * 新增
     *
     * @param lyLog 日志表
     * @return 结果
     */
    public Integer insertLyLog(LyLog lyLog);


    /**
     * 单个查询
     *
     * @param logId 日志id
     * @return 对象列表
     */
    public LyLog queryById(Long logId);


    /**
     * 选择性插入
     *
     * @param lyLog 日志表
     * @return 结果
     */
    public Integer insertSelective(LyLog lyLog);


    /**
     * 单个删除
     *
     * @param logId 日志id
     * @return 结果
     */
    public Integer deleteById(Long logId);


    /**
     * 通过主键修改数据
     *
     * @param lyLog 日志表
     * @return 结果
     */
    public Integer updateLyLog(LyLog lyLog);


    /**
     * 通过主键选择性修改
     *
     * @param lyLog 日志表
     * @return 结果
     */
    public Integer updateSelective(LyLog lyLog);

}
