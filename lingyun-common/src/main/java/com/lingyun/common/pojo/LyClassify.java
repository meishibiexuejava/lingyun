package com.lingyun.common.pojo;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * (LyClassify)表实体类
 *
 * @author 没事别学JAVA
 * @since 2021-07-25 00:49:17
 */
@Data
@ToString
@ApiModel(value = "LyClassify", description = "文章分类信息")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LyClassify implements Serializable {

    /**
     * 分类id
     */
    @TableId(value = "classify_id",type = IdType.AUTO)
    @ApiModelProperty(value = "分类id",example = "1")
    private Long classifyId;

    /**
     * 分类名字
     */
    @ApiModelProperty(value = "分类名字",hidden = true)
    @Field(type = FieldType.Text, store = true)
    private String classifyName;

    /**
     *创建时间
     */
    @ApiModelProperty(value = "创建时间",hidden = true)
    private Date createDate;
    /**
     *更新时间
     */
    @ApiModelProperty(value = "更新时间",hidden = true)
    private Date updateDate;




}
