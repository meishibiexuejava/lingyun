package com.lingyun.admin.controller.system.log;

import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.log.LogSearch;
import com.lingyun.system.service.LyLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
 * 日志管理
 *
 * @author 没事别学JAVA
 */
@RestController
@RequestMapping("/system/log")
@Api(value = "日志管理", tags = "日志管理")
public class LogController {



    @Resource
    LyLogService lyLogService;


    /**
     * 根据条件分页查询日志列表
     * @param logSearch 日志筛选条件
     * @return 结果
     */
    @ApiOperation(value = "根据条件分页查询日志列表")
    @PreAuthorize("hasRole('system:log:loglist')")
    @GetMapping("/logList")
    public AjaxResult logList(LogSearch logSearch){
        return AjaxResult.success("查询成功",lyLogService.logList(logSearch));
    }

    /**
     * 查看日志详情
     * @param logId 日志id
     * @return 结果
     */
    @ApiOperation(value = "查看日志详情")
    @ApiImplicitParam(name = "logId", value = "日志id", required = true)
    @PreAuthorize("hasRole('system:log:getlogbyid')")
    @GetMapping("/getLogById/{logId}")
    public AjaxResult getLogById(@PathVariable("logId") Long logId) {
        return AjaxResult.success(lyLogService.getById(logId));
    }


}
