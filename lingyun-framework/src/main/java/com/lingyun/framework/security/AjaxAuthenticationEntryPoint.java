package com.lingyun.framework.security;

import com.alibaba.fastjson.JSON;
import com.lingyun.common.utils.AjaxResult;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * 未认证用户访问未授权资源时抛出的异常
 *
 * 用户没有登录时访问没有授权的资源抛出的异常
 * @author 没事别学JAVA
 */
@Component
public class AjaxAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException{
        httpServletResponse.setHeader("Content-type", "application/json; charset=utf-8");
        httpServletResponse.getWriter().write(JSON.toJSONString(AjaxResult.warn("认证失败，请重新登录")));
    }

}
