package com.lingyun.common.pojo;

import java.io.Serializable;
import lombok.Data;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 文章分类关系表(LyArticleClassify)表实体类
 *
 * @author 没事别学JAVA
 * @since 2022-06-12 21:44:03
 */
@Data
@ToString
@TableName(value = "ly_article_classify")
public class LyArticleClassify implements Serializable {
    /**
     *文章id
     */
    private Long articleId;
    /**
     *分类id
     */
    private Long classifyId;
}