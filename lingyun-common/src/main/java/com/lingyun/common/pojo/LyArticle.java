package com.lingyun.common.pojo;

import java.util.Date;
import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * (LyArticle)表实体类
 * @author 没事别学JAVA
 *
 *
 *
 * @Document
 *        indexName                       索引库名
 *        useServerConfiguration          useServerConfiguration=true时，当Spring创建索引时,Spring不会在创建的索引中设置以下设置：shards,replicas,refreshInterval和indexStoreType.这些设置将是Elasticsearch默认值
 *        shards                          分片
 *        replicas                        副本    每个分区默认的备份数
 *        refreshInterval                 刷新间隔
 *        indexStoreType                  索引文件存储类型
 *        createIndex                     是否创建索引
 *        versionType
 *
 *
 * @Field
 *        type                             类型，可以是text、long、short、date、integer、object等
 *        index                            是否索引，默认为true
 *        store                            是否存储，默认为false
 *        analyzer                         指定分词器
 */
@Data
@ToString
@ApiModel(value = "LyArticle", description = "文章信息")
@TableName(value = "ly_article")
@Document(indexName = "article")
@Setting(

)
public class LyArticle implements Serializable {

    /**
     *文章id
     */
    @Id
    @Field(type = FieldType.Long, store = true)
    @ApiModelProperty(value = "文章id", example = "null")
    @TableId(value = "article_id" ,type = IdType.ASSIGN_ID)
    private Long articleId;
    /**
     *文章名字
     */
    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    @ApiModelProperty(value = "文章名称",example= "文章名称001")
    private String articleName;
    /**
     *文章关键字
     */
    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    @ApiModelProperty(value = "文章关键字",example= "文章关键字001")
    private String articleKeyword;
    /**
     *文章描述
     */
    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    @ApiModelProperty(value = "文章描述",example= "文章描述001")
    private String articleDescription;
    /**
     *缩略图(路径)
     */
    @Field(type = FieldType.Text, store = true)
    @ApiModelProperty(value = "缩略图(路径)",example = "/ly/ly-images/文章.png")
    private String articleThumbnail;
    /**
     *主题色（缩略图主题色）
     */
    @Field(type = FieldType.Text, store = true)
    @ApiModelProperty(value = "主题色（缩略图主题色）",example = "rgba(255, 0, 0, 1)")
    private String imgColor;
    /**
     *文章内容
     */
    @Field(type = FieldType.Text, store = true,analyzer = "ik_max_word")
    @ApiModelProperty(value = "文章内容",example = "文章内容001")
    private String articleContent;
    /**
     *是否被删除  0 正常  1 删除（逻辑删除，再回收站里等待彻底删除）
     */
    @ApiModelProperty(value = "是否被删除",example="0")
    private String delDelete;
    /**
     *状态 0暂存 1正常 2隐藏 3违规
     */
    @ApiModelProperty(value = "状态",example = "1")
    private String articleStatus;
    /**
     *是否轮播 0 不轮播 1轮播
     */
    @Field(type = FieldType.Text, store = true)
    @ApiModelProperty(value = "是否轮播 0 不轮播 1轮播",example = "1")
    private String isShuffling;

    /**
     *是否推广 0不推广  1推广
     */
    @Field(type = FieldType.Text, store = true)
    @ApiModelProperty(value = "是否推广 0不推广  1推广",example = "1")
    private String isPopularize;

    /**
     *创建时间
     */
    @Field(type = FieldType.Date, store = true, format = DateFormat.date_hour_minute_second, pattern = "yyyy-MM-dd HH:mm:ss")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间",hidden = true)
    private Date createDate;
    /**
     *最后更新时间
     */
    @ApiModelProperty(value = "最后更新时间",hidden = true)
    private Date updateDate;

    /**
     * 附加属性
     */
    @Field(type = FieldType.Object, store = true)
    @TableField(exist = false)
    @ApiModelProperty(value = "分类")
    LyClassify lyClassify;


}
