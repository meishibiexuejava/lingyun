package com.lingyun.common.vo.user;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.lingyun.common.pojo.LyRole;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserRole implements Serializable {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 账号
     */
    private String userName;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 邮箱
     */
    private String userEmail;
    /**
     * 手机号码
     */
    private String phonenumber;
    /**
     * 性别 （0男 1女 2未知）
     */
    private String userSex;
    /**
     * 账号状态（0正常 1停用）
     */
    private String userStatus;
    /**
     * 角色名称
     */
    private List<LyRole> roleName;
}
