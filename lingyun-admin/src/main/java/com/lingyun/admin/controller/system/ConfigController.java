package com.lingyun.admin.controller.system;

import com.lingyun.common.pojo.LyConfig;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.system.service.LyConfigService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 系统配置
 *
 * @author meishibiexuejava
 * 2024/4/23 18:41
 */
@RestController
@RequestMapping("/system/config")
@Api(value = "系统配置", tags = "系统配置")
public class ConfigController {

    @Resource
    LyConfigService lyricConfigService;


    public AjaxResult inquireConfigKeyValue(LyConfig config) {
        return AjaxResult.success(lyricConfigService.getById(config.getConfigId()));
    }



}
