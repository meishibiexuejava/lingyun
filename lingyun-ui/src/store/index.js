// 引入vue
import Vue from 'vue'
// 引入vuex
import Vuex from 'vuex'


// 引入自定义getters
import getters from '@/store/getters'



// 许可路由
import permission from './permission'
// 用户登录
import user from './user'
// 标签试图（tags）
import tagsView from "@/store/tagsView";
// 系统全局设置
import settings from "@/store/settings";
// 侧边栏操作
import sidebarOperation from "@/store/sidebarOperation"


// 应用vuex
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    sidebarOperation,
    settings,
    user,
    tagsView,
    permission
  },
  getters
})
export default store
