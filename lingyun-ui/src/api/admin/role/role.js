import request from '@/utils/request'


/**
 * 查询角色列表（下拉框）
 * @returns {*}
 */
export function selectOption() {

    return request({
        url: '/system/role/selectOption',
        method: 'get',
    })

}

/**
 * 根据条件分页查询角色列表
 * @param query
 * @returns {*}
 */
export function roleList(query) {

    return request({
        url: '/system/role/roleList',
        method: 'get',
        params: query
    })

}


/**
 * 添加角色
 * @param data
 * @returns {*}
 */
export function addRole(data) {

    return request({
        url: '/system/role/addRole',
        method: 'post',
        data: data
    })

}


/**
 * 根据角色ID查询角色
 * @param roleId 角色id
 * @returns {*}
 */
export function selectRoleByid(roleId) {

    return request({
        url: '/system/role/selectRoleById/' + roleId,
        method: 'get'
    })

}


/**
 * 修改角色
 * @param data
 * @returns {*}
 */
export function updateRole(data) {

    return request({
        url: '/system/role/updateRole',
        method: 'put',
        data: data
    })

}


/**
 * 删除角色
 * @param roleId 角色id
 * @returns {*}
 */
export function deleteRoleById(roleId) {

    return request({
        url: '/system/role/deleteRoleById/' + roleId,
        method: 'delete',
    })

}


/**
 * 批量删除角色
 * @param ids 角色id 集合
 * @returns {*}
 */
export function deleteRoleByIds(ids) {

    return request({
        url: '/system/role/deleteRoleByIds',
        method: 'delete',
        data: ids
    })

}


/**
 * 导出角色信息(Excel)
 * @param query  筛选条件
 * @returns {*}
 */
export function roleExportExcel(query) {

    return request({
        url: '/system/role/roleExportExcel',
        method: 'get',
        params: query
    })

}


/**
 * 修改角色状态
 * @param roleId 角色id
 * @param roleStatus 角色状态
 * @returns {*}
 */
export function updateRoleStatus(roleId, roleStatus) {

    const data = {
        roleId,
        roleStatus
    }
    return request({
        url: '/system/role/updateRoleStatus',
        method: 'put',
        data: data
    })

}



