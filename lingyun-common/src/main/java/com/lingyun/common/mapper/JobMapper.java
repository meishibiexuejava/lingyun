package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.JobAndTrigger;
import org.apache.ibatis.annotations.Param;

/**
 * @author 没事别学JAVA
 * 2021/4/27 18:07
 */
public interface JobMapper {


    /**
     * 获取定时任务列表
     * @param page 分页
     * @return 获取定时任务列表
     */
    public IPage<JobAndTrigger> quartzList(IPage<JobAndTrigger> page);


}
