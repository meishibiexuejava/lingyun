import request from '@/utils/request'


/**
 * 根据条件分页查询用户列表
 * @param query
 * @returns {*}
 */
export function getUserList(query) {

    return request({
        url: '/system/user/getUserList',
        method: 'get',
        params: query
    })

}

/**
 * 添加用户
 * @param data 用户信息
 * @returns {*}
 */
export function addUser(data) {

    return request({
        url: '/system/user/addUser',
        method: 'post',
        data: data
    })

}

/**
 * 根据id查询用户详细信息
 * @param userId  用户id
 * @returns {*}
 */
export function selectUserById(userId) {

    return request({
        url: '/system/user/selectUserById/' + userId,
        method: 'get'
    })

}

/**
 * 修改用户
 * @param data 用户信息
 * @returns {*}
 */
export function updateUser(data) {

    return request({
        url: '/system/user/updateUser',
        method: 'put',
        data: data
    })

}

/**
 * 删除用户
 * @param userId 用户id
 * @returns {*}
 */
export function deletUserById(userId) {

    return request({
        url: '/system/user/deletUserById/' + userId,
        method: 'delete',
    })

}

/**
 * 批量删除用户
 * @param ids 用户id(list)
 * @returns {*}
 */
export function deletUserByIds(ids) {

    return request({
        url: '/system/user/deletUserByIds',
        method: 'delete',
        data: ids
    })

}

/**
 * 导出用户信息(Excel)
 * @param query
 * @returns {*}
 */
export function userExportExcel(query) {

    return request({
        url: '/system/user/userExportExcel',
        method: 'get',
        params: query
    })

}


