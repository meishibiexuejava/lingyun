package com.lingyun.common.annotation;

import java.lang.annotation.*;

@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    /**
     * 模块
     */
    public String title() default "";

    /**
     * 日志业务类型（0查询 1 新增 2修改 3删除 4上传图片 5其他 6查看 7导出 8绑定 9取消绑定 10后台登录 11退出登录 12后台注册  50访问web首页 51查看web新闻详情页）
     */
    public String businessType() default "";

    /**
     * 日志描述
     */
    public String description() default "";


}
