package com.lingyun.common.pojo;

import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 日志表(LyLog)
 *
 * @author 没事别学JAVA
 * @since 2023-03-28 23:35:56
 */
@Data
@ToString
@TableName(value = "ly_log")
@ApiModel(value = "LyLog对象",description = "日志表(LyLog)")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LyLog implements Serializable {

    /**
     *日志id
     */
    @TableId(value = "log_id",type = IdType.AUTO)
    @ApiModelProperty(value = "日志id")
    private Long logId;

    /**
     *日志模块
     */
    @ApiModelProperty(value = "日志模块")
    private String logTitle;

    /**
     *日志业务类型（0查询 1 新增 2修改 3删除 4上传图片 5其他 6查看 7导出 8绑定 9取消绑定 10后台登录 11退出登录 12后台注册  50访问web首页 51查看web新闻详情页）
     */
    @ApiModelProperty(value = "日志业务类型（0查询 1 新增 2修改 3删除 4上传图片 5其他 6查看 7导出 8绑定 9取消绑定 10后台登录 11退出登录 12后台注册  50访问web首页 51查看web新闻详情页）")
    private String logBusinessType;

    /**
     *请求方式
     */
    @ApiModelProperty(value = "请求方式")
    private String requestMode;

    /**
     *方法名称
     */
    @ApiModelProperty(value = "方法名称")
    private String methodName;

    /**
     *操作人员
     */
    @ApiModelProperty(value = "操作人员")
    private String logOperationName;

    /**
     *操作人员昵称
     */
    @ApiModelProperty(value = "操作人员昵称")
    private String logOperationNickName;

    /**
     *用户类型
     */
    @ApiModelProperty(value = "用户类型")
    private String userType;

    /**
     *请求路径（URL）
     */
    @ApiModelProperty(value = "请求路径（URL）")
    private String logOperationUrl;

    /**
     *浏览器
     */
    @ApiModelProperty(value = "浏览器")
    private String browser;

    /**
     *访问ip
     */
    @ApiModelProperty(value = "访问ip")
    private String logOperationIp;

    /**
     *操作地点
     */
    @ApiModelProperty(value = "操作地点")
    private String logOperLocation;

    /**
     *系统
     */
    @ApiModelProperty(value = "系统")
    private String os;

    /**
     *返回参数
     */
    @ApiModelProperty(value = "返回参数")
    private String jsonResult;

    /**
     *错误消息
     */
    @ApiModelProperty(value = "错误消息")
    private String errorMsg;

    /**
     *操作状态（1正常 2异常）
     */
    @ApiModelProperty(value = "操作状态（1正常 2异常）")
    private String logOperationStatus;

    /**
     *日志描述
     */
    @ApiModelProperty(value = "日志描述")
    private String logDescription;

    /**
     *操作时间
     */
    @ApiModelProperty(value = "操作时间")
    private Date logOperationDate;


}
