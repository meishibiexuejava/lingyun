
<p align="center">
	<img alt="logo" src="https://meishibiexuejava.cn/lingyun-page/logo.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">LingYun v1.0.0</h1>
<h4 align="center">基于SpringBoot+Vue前后端分离的新闻管理系统</h4>
<p align="center">
    <a href="https://gitee.com/Mr-zkc/lingyun/stargazers"><img src='https://gitee.com/Mr-zkc/lingyun/badge/star.svg?theme=dark' alt='star'/></a>
	<a href="https://gitee.com/Mr-zkc/lingyun/members"><img src='https://gitee.com/Mr-zkc/lingyun/badge/fork.svg?theme=dark' alt='fork'/></a>
    <a href="https://gitee.com/Mr-zkc/lingyun"><img src="https://img.shields.io/badge/Lingyun-1.0.0-brightgreen"></a>
	<a href="https://gitee.com/Mr-zkc/lingyun/blob/master/LICENSE"><img src="https://img.shields.io/badge/license-MulanPSL--2.0-brightgreen"></a>
</p>

## 系统简介
- 前端采用Vue、Element UI。
- 后端采用SpringBoot、MybatisPlus、SpringSecurity、Redis、Jwt、Quartz、EasyPoi、Elasticsearch、Solr、EasyCaptcha、hutool、Swaggerui
- 权限认证基于SpringSecurity+Redis+Jwt 实现RBAC权限管理

## 项目环境部署部署（基于docker）
<table>
    <tr>
        <td>名字</td>
        <td>版本</td>
        <td>参考连接</td>
    </tr>
    <tr>
        <td>Mysql：</td>
        <td>8.0.16</td>
        <td>https://blog.csdn.net/qq_41980849/article/details/126085050</td>
    </tr>
    <tr>
        <td>Redsi：</td>
        <td>7.0.0</td>
        <td>https://blog.csdn.net/qq_41980849/article/details/126084645</td>
    </tr>
    <tr>
        <td>Elasticsearch：</td>
        <td>7.13.4</td>
        <td>https://blog.csdn.net/qq_41980849/article/details/126033914</td>
    </tr>
    <tr>
        <td>Kibana：</td>
        <td>7.13.4</td>
        <td>https://blog.csdn.net/qq_41980849/article/details/126034355</td>
    </tr>
</table>

## 项目访问
<table>
    <tr>
        <td>前台首页：</td>
        <td>http://localhost/</td>
    </tr>
    <tr>
        <td>后台登录：</td>
        <td>http://localhost/login</td>
    </tr>
</table>

## 界面展示
<table>
    <tr>
        <td><img src="https://meishibiexuejava.cn/lingyun-page/web-首页.png"/></td>
        <td><img src="https://meishibiexuejava.cn/lingyun-page/web-详情页.png"/></td>
    </tr>
    <tr>
        <td><img src="https://meishibiexuejava.cn/lingyun-page/admin-登录页.png"/></td>
        <td><img src="https://meishibiexuejava.cn/lingyun-page/admin-首页.png"/></td>
    </tr>
        <tr>
        <td><img src="https://meishibiexuejava.cn/lingyun-page/admin-用户管理.png"/></td>
        <td><img src="https://meishibiexuejava.cn/lingyun-page/admin-角色管理.png"/></td>
    </tr>
        <tr>
        <td><img src="https://meishibiexuejava.cn/lingyun-page/admin-菜单管理.png"/></td>
        <td><img src="https://meishibiexuejava.cn/lingyun-page/admin-文章管理.png"/></td>
    </tr>
        <tr>
        <td><img src="https://meishibiexuejava.cn/lingyun-page/admin-服务监控.png"/></td>
        <td></td>
    </tr>
</table>

<p align="center">
	<a href=""><img src="https://gitee.com/Mr-zkc/lingyun/widgets/widget_card.svg?colors=393222,ebdfc1,fffae5,d8ca9f,393222,a28b40"></a>
</p>

