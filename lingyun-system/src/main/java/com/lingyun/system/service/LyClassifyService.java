package com.lingyun.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.pojo.LyClassify;

import java.util.List;

public interface LyClassifyService extends IService<LyClassify> {

    public List<LyClassify> queryAllClassifyList();


}
