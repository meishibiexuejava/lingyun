package com.lingyun.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.exception.ServiceException;
import com.lingyun.common.exception.user.CaptchaException;
import com.lingyun.common.exception.user.CaptchaExpireException;
import com.lingyun.common.exception.user.UserAuthenticationException;
import com.lingyun.common.exception.user.UserPasswordNotMatchException;
import com.lingyun.common.vo.*;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.exception.LyException;
import com.lingyun.common.pojo.LyUser;
import com.lingyun.common.vo.role.AllocationUserCondition;
import com.lingyun.common.vo.user.UserAll;
import com.lingyun.common.vo.user.UserFrom;
import com.lingyun.common.vo.UserRegister;
import com.lingyun.common.vo.user.UserQueryCriteria;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * (LyUser)表服务接口
 *
 * @author 没事别学JAVA
 * @since 2021-03-07 17:06:47
 */
public interface LyUserService extends IService<LyUser> {
    /**
     * 邮箱注册-发送验证码
     */
    void SendEmailVerificationCode(String Email) throws LyException;

    /**
     * 邮箱注册
     */
    void register(UserRegister userRegister) throws LyException;

    /**
     *
     * 登录
     */
    String login(LoginFrom loginFrom, HttpServletRequest request) throws CaptchaExpireException, CaptchaException, UserPasswordNotMatchException, ServiceException, UserAuthenticationException;


    /**
     * 根据条件分页查询用户列表
     *
     * @param userQueryCriteria 用户筛选条件
     * @return 用户列表
     */
    IPage<UserAll> getUserList(UserQueryCriteria userQueryCriteria);

    /**
     * 添加用户
     *
     * @param userFrom 用户信息
     * @return 结果
     */
    public boolean addUser(UserFrom userFrom) throws LyException;

    /**
     * 根据id查询用户详细信息
     *
     * @return 用户信息
     */
    public UserFrom selectUserById(Long userId);

    /**
     * 根据用户id 获取角色列表
     *
     * @param userid 用户id
     * @return 角色id（list）
     */
    List<Long> getUserRole(Long userid);

    /**
     * 修改用户
     *
     * @param userFrom 用户信息
     * @return 结果
     */
    public Boolean updateUser(UserFrom userFrom);


    /**
     * 导出用户信息(Excel)
     *
     * @param userQueryCriteria 用户筛选条件
     * @return 文件名字
     */
    public List<UserAll> userExportExcel(UserQueryCriteria userQueryCriteria);


    /**
     * 获取授权用户列表
     *
     * @param allocationUserCondition 分配用户搜索条件
     * @return 结果
     */
    public IPage<UserAll> authorizationUserList(AllocationUserCondition allocationUserCondition);


}
