package com.lingyun.common.exception.user;
import com.lingyun.common.utils.MessageUtils;

/**
 * 验证码错误异常类
 *
 * @author ruoyi
 */
public class CaptchaException extends UserException {
    private static final long serialVersionUID = 1L;

    public CaptchaException() {
        super(MessageUtils.message("user.jcaptcha.error"));
    }
}
