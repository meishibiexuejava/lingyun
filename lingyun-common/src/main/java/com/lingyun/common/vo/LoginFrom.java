package com.lingyun.common.vo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 用户登录表单
 * @author 没事别学JAVA
 */
@Data
@ToString
@ApiModel(value = "LoginFrom",description = "用户登录表单")
public class LoginFrom implements Serializable {
    /**
     * 账号
     */
    @ApiModelProperty(value = "账号",required = true)
    private String userName;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码",required = true)
    private String password;
    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码",required = true)
    private String verificationCode;
    /**
     * redis中的验证码的key
     *
     * */
    @ApiModelProperty(value = "redis中的验证码的key",required = true)
    private String keyCode;
}


