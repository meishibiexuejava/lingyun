package com.lingyun.common.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目配置
 *
 */

@Setter
@Component
@ConfigurationProperties(prefix = "lingyun")
public class LingYunConfig {
    /**
     *
     * 项目名称
     */
    @Getter
    private String name;
    /**
     *
     * 图片上传路径
     */
    @Getter
    private static String imagePath;

    /**
     * 文件下载路径（文件下载到本机的路径）
     */
    @Getter
    private static String filePath;


    public void setImagePath(String imagePath) {
        LingYunConfig.imagePath = imagePath;
    }

    public void setFilePath(String filePath) {
        LingYunConfig.filePath = filePath;
    }

}
