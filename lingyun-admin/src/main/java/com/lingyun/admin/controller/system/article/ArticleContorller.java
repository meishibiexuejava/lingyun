package com.lingyun.admin.controller.system.article;

import com.lingyun.common.annotation.Log;
import com.lingyun.common.pojo.LyArticle;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.article.ArticleSearch;
import com.lingyun.system.service.LyArticleService;
import com.lingyun.system.service.LyClassifyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文章管理
 *
 * @author 没事别学JAVA
 */
@RestController
@RequestMapping("/system/article")
@Api(value = "文章管理", tags = "文章管理")
public class ArticleContorller {

    @Resource
    LyArticleService lyArticleService;

    @Resource
    LyClassifyService lyClassifyService;

    /**
     * 根据条件分页查询文章列表
     *
     * @param articleSearch 文章条件搜索表单
     * @return 结果
     */
    @ApiOperation(value = "根据条件分页查询文章列表")
    @Log(title = "文章模块", businessType = "0", description = "根据条件分页查询文章列表")
    @PreAuthorize("hasRole('system:article:articlelist')")
    @GetMapping("/articleList")
    public AjaxResult articleList(ArticleSearch articleSearch) {
        return AjaxResult.success("查询成功", lyArticleService.articleList(articleSearch));
    }


    /**
     * 添加文章
     *
     * @param lyArticle 文章表单
     * @return 结果
     */
    @ApiOperation(value = "添加文章")
    @Log(title = "文章模块", businessType = "1", description = "添加文章")
    @PreAuthorize("hasRole('system:article:insertarticle')")
    @PostMapping("/insertArticle")
    public AjaxResult insertArticle(@RequestBody LyArticle lyArticle) {
        if (lyArticleService.insertArticle(lyArticle) > 0) {
            return AjaxResult.success("成功");
        }
        return AjaxResult.success("添加失败");
    }


    /**
     * 根据id查看文章
     *
     * @param articleId 文章id
     * @return 结果
     */
    @ApiOperation(value = "根据id查看文章")
    @ApiImplicitParam(name = "articleId", value = "文章id", required = true)
    @PreAuthorize("hasRole('system:article:getarticlebyid')")
    @GetMapping("/getArticleById/{articleId}")
    public AjaxResult getArticleById(@PathVariable("articleId") Long articleId) {
        return AjaxResult.success(lyArticleService.getArticleById(articleId));
    }


    /**
     * 修改文章信息
     *
     * @param lyArticle 文章实体类
     * @return 结果
     */
    @ApiOperation(value = "修改文章信息")
    @Log(title = "文章模块", businessType = "2", description = "修改文章信息")
    @PreAuthorize("hasRole('system:article:updatearticle')")
    @PutMapping("/updateArticle")
    public AjaxResult updateArticleId(@RequestBody LyArticle lyArticle) {
        if (lyArticleService.updateArticleId(lyArticle) > 0) {
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.success("修改失败");
    }


    /**
     * 删除文章
     *
     * @param articleId 文章id
     * @return 结果
     */
    @ApiOperation(value = "删除文章")
    @ApiImplicitParam(name = "articleId", value = "文章id", required = true)
    @Log(title = "文章模块", businessType = "3", description = "删除文章")
    @PreAuthorize("hasRole('system:article:deletearticlebyid')")
    @DeleteMapping("/deleteArticleById/{articleId}")
    public AjaxResult deleteArticleById(@PathVariable("articleId") Long articleId) {
        if (lyArticleService.deleteArticleById(articleId)) {
            return AjaxResult.success("删除成功");
        } else {
            return AjaxResult.success("删除失败");
        }
    }


    /**
     * 批量删除文章
     *
     * @param articleIds 文章id(list)
     * @return 结果
     */
    @ApiOperation(value = "批量删除文章")
    @ApiImplicitParam(name = "articleIds", value = "文章id(list)", required = true)
    @Log(title = "文章模块", businessType = "3", description = "批量删除文章")
    @PreAuthorize("hasRole('system:article:deletearticlebyids')")
    @DeleteMapping("/deleteArticleByIds")
    public AjaxResult deleteArticleByIds(@RequestBody List<Long> articleIds) {

        if (lyArticleService.deleteArticleByIds(articleIds)) {
            return AjaxResult.success("删除成功");
        } else {
            return AjaxResult.success("删除失败");
        }

    }


    /**
     * 轮播图状态修改
     *
     * @param lyArticle 只有 文章id 轮播图状态
     * @return 结果
     */
    @ApiOperation(value = "轮播图状态修改", notes = "只包含：文章id(articleId)、轮播图状态（isShuffling）")
    @Log(title = "文章模块", businessType = "2", description = "轮播图状态修改")
    @PreAuthorize("hasRole('system:article:updateisshuffling')")
    @PutMapping("/updateIsShuffling")
    public AjaxResult updateIsShuffling(@RequestBody LyArticle lyArticle) {

        if (lyArticleService.updateById(lyArticle)) {
            return AjaxResult.success("修改成功");
        } else {
            return AjaxResult.success("修改失败");
        }

    }

    /**
     * 推广状态修改
     *
     * @param lyArticle 只有 文章id  推广状态
     * @return 结果
     */
    @ApiOperation(value = "推广状态修改", notes = "只包含：文章id(articleId)、推广状态(isPopularize)")
    @Log(title = "文章模块", businessType = "2", description = "推广状态修改")
    @PreAuthorize("hasRole('system:article:updateispopularize')")
    @PutMapping("/updateIsPopularize")
    public AjaxResult updateIsPopularize(@RequestBody LyArticle lyArticle) {

        if (lyArticleService.updateById(lyArticle)) {
            return AjaxResult.success("修改成功");
        } else {
            return AjaxResult.success("修改失败");
        }

    }


    /**
     * 获取文章分类列表（下拉框使用）
     *
     * @return 结果
     */
    @ApiOperation(value = "获取文章分类列表（下拉框使用）")
    @GetMapping("/queryAllClassifyList")
    public AjaxResult queryAllClassifyList() {
        return AjaxResult.success(lyClassifyService.queryAllClassifyList());
    }


}
