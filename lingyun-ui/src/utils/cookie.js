import Cookies from 'js-cookie'

const Key = 'LingYun-Token'

export function getToken() {
  return Cookies.get(Key)
}

export function setToken(token) {
  return Cookies.set(Key, token)
}

export function removeToken() {
  return Cookies.remove(Key)
}
