package com.lingyun.common.vo.role;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 角色最新状态
 *
 * @author meishibiexuejava
 * 2023/4/8 23:24
 */
@Data
@ToString
@ApiModel(value = "UpdateRoleStatus", description = "角色最新状态")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UpdateRoleStatus implements Serializable {


    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID", example = "null")
    private Long roleId;

    /**
     * 角色状态（0正常 1停用）
     */
    @ApiModelProperty(value = "角色状态（0启用 1停用）", example = "0", required = true)
    private String roleStatus;

}
