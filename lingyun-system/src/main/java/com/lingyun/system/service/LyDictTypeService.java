package com.lingyun.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.exception.dict.DictHaveDataException;
import com.lingyun.common.pojo.LyDictType;
import com.lingyun.common.vo.dict.DictPage;

import java.util.List;

/**
 * (LyDictType)表服务接口
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:18:48
 */
public interface LyDictTypeService extends IService<LyDictType> {


    /**
     * 根据条件分页查询字典类型列表
     *
     * @param dictPage 字典筛选条件
     * @return 结果
     */
    IPage<LyDictType> dictList(DictPage dictPage);

    /**
     * 根据字典类型id查询字典类型信息
     *
     * @param dictId 字典类型id
     * @return 字典类型信息
     */
    public LyDictType selectDictById(Long dictId);

    /**
     * 删除字典类型
     *
     * @param dictId 字典类型id
     * @return 结果
     */
    public Integer deleteDictTypeById(Long dictId) throws DictHaveDataException;

    /**
     * 批量删除字典类型
     *
     * @param ids 字典类型id（list）
     * @return 结果
     */
    public Boolean deleteDictTypeByIds(List<Long> ids) throws DictHaveDataException;

    /**
     * 获取字典类型名称(下拉框使用)
     *
     * @return 结果
     */
    public List<LyDictType> selectDictTypeAll();

}
