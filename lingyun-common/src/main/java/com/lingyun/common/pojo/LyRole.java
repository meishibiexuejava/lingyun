package com.lingyun.common.pojo;

import java.util.Date;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 角色信息表(LyRole)表实体类
 *
 * @author 没事别学JAVA
 * @since 2021-03-25 12:39:52
 */
@Data
@ToString
@ApiModel(value = "LyRole", description = "角色信息")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LyRole implements Serializable {

    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID",example = "null")
    @Excel(name = "角色ID")
    @TableId(value = "role_id", type = IdType.AUTO)
    private Long roleId;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称", required = true)
    @Excel(name = "角色名称")
    private String roleName;

    /**
     * 角色权限字符串
     */
    @ApiModelProperty(value = "角色权限字符串", required = true)
    @Excel(name = "角色权限字符串")
    private String roleKey;

    /**
     * 数据范围（1：全部数据权限 2：自定数据权限 3：角色数据权限
     */
    @ApiModelProperty(value = "数据范围（1：全部数据权限 2：自定数据权限 3：角色数据权限", hidden = true)
    @Excel(name = "数据范围", replace = {"全部数据权限_1", "自定数据权限_2", "角色数据权限_3"})
    private String dataScope;

    /**
     * 角色状态（0正常 1停用）
     */
    @ApiModelProperty(value = "角色状态（0启用 1停用）",example = "0", required = true)
    @Excel(name = "角色状态（0启用 1停用）", replace = {"启用_0", "停用_1"})
    private String roleStatus;

    /**
     * 删除标志（0正常 2删除）
     */
    @ApiModelProperty(value = "删除标志（0正常 2删除）", hidden = true)
    @Excel(name = "删除标志（0正常 2删除）", replace = {"正常_0", "删除_2"})
    private String delFlag;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", hidden = true)
    @Excel(name = "创建时间", databaseFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", hidden = true)
    @Excel(name = "更新时间", databaseFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 菜单id(数组)
     */
    @ApiModelProperty(value = "菜单id(数组)", example = "[2, 5, 25, 1, 19, 29, 4]")
    @TableField(exist = false)
    private Long[] menuIds;


    public LyRole(Long roleId, String roleStatus) {
        this.roleId = roleId;
        this.roleStatus = roleStatus;
    }

}
