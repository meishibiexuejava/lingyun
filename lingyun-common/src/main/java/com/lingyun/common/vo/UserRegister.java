package com.lingyun.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 用户注册表单
 * @author 没事别学JAVA
 */
@Data
@ToString
@ApiModel(value = "UserRegister",description = "用户注册表单")
public class UserRegister {

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String userName;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;


}
