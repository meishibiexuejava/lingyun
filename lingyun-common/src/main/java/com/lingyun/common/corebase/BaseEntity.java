package com.lingyun.common.corebase;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@ToString
public class BaseEntity implements Serializable {

    /**
     * 拼接sql(数据权限)
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "拼接sql(数据权限)", hidden = true)
    private Map<String, Object> params;

    public Map<String, Object> getParams() {
        if (params == null) {
            params = new HashMap<>();
        }
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }



}
