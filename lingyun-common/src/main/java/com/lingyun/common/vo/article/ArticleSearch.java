package com.lingyun.common.vo.article;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@ApiModel(value = "ArticleSearch", description = "文章筛选条件")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ArticleSearch implements Serializable {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private Integer current;
    /**
     * 页大小
     */
    @ApiModelProperty(value = "页大小", required = true)
    private Integer size;

    /**
     * 文章名称
     */
    @ApiModelProperty(value = "文章名称")
    private String articleName;
    /**
     * 文章状态（0暂存 1正常 2隐藏 3违规）
     */
    @ApiModelProperty(value = "文章状态（0暂存 1正常 2隐藏 3违规）")
    private String articleStatus;

    /**
     * 分类（id）
     */
    @ApiModelProperty(value = "分类（id）")
    private Long classifyId;


}
