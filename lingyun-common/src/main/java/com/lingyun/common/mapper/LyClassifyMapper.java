package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lingyun.common.pojo.LyClassify;

import java.util.List;

public interface LyClassifyMapper extends BaseMapper<LyClassify> {

    public List<LyClassify> queryAllClassifyList();

}
