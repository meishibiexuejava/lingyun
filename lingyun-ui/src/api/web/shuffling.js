import request from '@/utils/request'


/**
 * 文章分页查询
 * @returns {*}
 */
export function shufflingFigure() {

    return request({
        url: '/web/es/shufflingFigure',
        method: 'get',
    })

}