package com.lingyun.common.corebase;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@ApiModel(value = "GeneralEntity", description = "角色筛选条件")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GeneralEntity extends BaseEntity {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private Integer current;
    /**
     * 页大小
     */
    @ApiModelProperty(value = "页大小", required = true)
    private Integer size;


    // 角色列表搜索
    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    private String roleName;
    /**
     * 权限字符
     */
    @ApiModelProperty(value = "权限字符")
    private String roleKey;

    /**
     * 状态
     */
    @ApiModelProperty(value = "角色状态（0启用 1停用）")
    private String roleStatus;


}
