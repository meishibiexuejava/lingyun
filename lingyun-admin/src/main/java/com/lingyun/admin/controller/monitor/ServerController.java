package com.lingyun.admin.controller.monitor;

import com.lingyun.common.annotation.Log;
import com.lingyun.common.utils.AjaxResult;
import com.lingyun.common.vo.monitor.server.Server;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统监控
 *
 * @author 没事别学JAVA
 */
@RestController
@RequestMapping("/monitor")
@Api(value = "系统监控", tags = "系统监控")
public class ServerController {


    @ApiOperation(value = "获取服务器信息")
    @Log(title = "监控模块", businessType = "6", description = "获取服务器信息")
    @PreAuthorize("hasRole('monitor:server')")
    @GetMapping("/server")
    public AjaxResult server(){
        Server server=new Server();
        try {
            server.copyTo();
        } catch (Exception e) {
            return AjaxResult.success(e.getMessage());
        }
        return AjaxResult.success(server);
    }




}
