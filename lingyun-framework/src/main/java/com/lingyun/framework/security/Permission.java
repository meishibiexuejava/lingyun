package com.lingyun.framework.security;
import com.lingyun.system.service.LyMenuService;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;

/**
 * @author 没事别学JAVA
 * 2021/12/14 0:49
 */
@Component
public class Permission {

    @Resource
    LyMenuService lyNavService;

    /**
     * 获取菜单数据权限
     *
     * @param  UseId 用户id
     * @param  roles 当前用户的角色
     * @return 菜单权限信息
     */
    public Set<String> getMenuPermission(Long UseId,Set<String> roles) {
        Set<String> perms = new HashSet<String>();
        if(roles.contains("admin")){
            //perms.add("*:*:*");
            perms.addAll(lyNavService.selectAdminUserPermsAll());
        } else {
            perms.addAll(lyNavService.selectUserPermsByUserId(UseId));
        }
        return perms;
    }



}
