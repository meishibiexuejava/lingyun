package com.lingyun.common.vo.user;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.lingyun.common.pojo.LyRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 用户信息表单
 * @author 没事别学JAVA
 * 2021/11/20 3:21
 */
@Data
@ToString
@ApiModel(value = "UserFrom",description = "用户信息")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserFrom implements Serializable {
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID",example = "null")
    private Long userId;
    /**
     * 账号
     */
    @ApiModelProperty(value = "账号",required = true)
    private String userName;
    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称",required = true)
    private String nickName;
    /**
     * 用户类型（0系统用户 1注册用户）
     */
    @ApiModelProperty(value = "用户类型（0系统用户 1注册用户）",required = true, example = "0")
    private String userType;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String userEmail;
    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    private String phonenumber;
    /**
     * 性别 （0男 1女 2未知）
     */
    @ApiModelProperty(value = "性别 （0男 1女 2未知）",required = true, example = "0")
    private String userSex;
    /**
     * 头像路径
     */
    @ApiModelProperty(value = "头像路径",required = true, example = "/ly/ly-images/凌云.png")
    private String avatar;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码",required = true)
    private String password;
    /**
     * 账号状态（0正常 1停用）
     */
    @ApiModelProperty(value = "账号状态（0正常 1停用）",required = true, example = "0")
    private String userStatus;
    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id",required = true, example = "[1]")
    private Long[] roleIds;

}
