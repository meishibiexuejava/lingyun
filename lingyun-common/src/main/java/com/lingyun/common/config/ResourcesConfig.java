package com.lingyun.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/**
 * 通用配置
 *
 * @author 没事别学JAVA
 */
@Configuration
public class ResourcesConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //registry.addResourceHandler("/ly-images/**").addResourceLocations("file:/ly-images/","/static/");
        registry.addResourceHandler("/ly/ly-images/**").addResourceLocations("file:/ly/ly-images/");
    }


    /**
     *
     * 跨域配置
     *
     */
    @Bean
    public CorsFilter corsFilter() {

        CorsConfiguration config = new CorsConfiguration();
        // 允许带凭证
        config.setAllowCredentials(true);
        // 设置访问源地址
        config.addAllowedOrigin("*");
        // 设置访问源请求方法
        config.addAllowedMethod("*");
        // 设置访问源请求头
        config.addAllowedHeader("*");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        // 对所有url都生效
        source.registerCorsConfiguration("*", config);
        return new CorsFilter(source);

    }





}
