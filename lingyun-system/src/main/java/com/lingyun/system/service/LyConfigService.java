package com.lingyun.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.pojo.LyConfig;

/**
 * (LyConfig)表服务接口
 *
 *
 * @author 没事别学JAVA
 * @since 2024-04-23 18:33:16
 */
public interface LyConfigService extends IService<LyConfig> {

}
