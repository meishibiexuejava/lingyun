package com.lingyun.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.lingyun.common.exception.LyException;
import com.lingyun.common.exception.ServiceException;
import com.lingyun.common.exception.user.CaptchaException;
import com.lingyun.common.exception.user.CaptchaExpireException;
import com.lingyun.common.exception.user.UserAuthenticationException;
import com.lingyun.common.exception.user.UserPasswordNotMatchException;
import com.lingyun.common.mapper.LyRoleMapper;
import com.lingyun.common.mapper.LyUserRoleMapper;
import com.lingyun.common.pojo.LyUser;
import com.lingyun.common.pojo.LyUserRole;
import com.lingyun.common.utils.ErrorCode;
import com.lingyun.common.utils.RegexVerification;
import com.lingyun.common.utils.StringUtils;
import com.lingyun.common.utils.jwt.JwtUtils;
import com.lingyun.common.utils.redis.RedisUtils;
import com.lingyun.common.mapper.LyUserMapper;
import com.lingyun.common.vo.*;
import com.lingyun.common.vo.role.AllocationUserCondition;
import com.lingyun.common.vo.user.UserAll;
import com.lingyun.common.vo.user.UserFrom;
import com.lingyun.common.vo.UserRegister;
import com.lingyun.common.vo.user.UserQueryCriteria;
import com.lingyun.system.api.EmailService;
import com.lingyun.system.service.LyUserService;
import org.springframework.beans.BeanUtils;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * @author 没事别学JAVA
 * @since 2021-03-07 17:06:47
 */
@Service("lyUserService")
public class LyUserServiceImpl extends ServiceImpl<LyUserMapper, LyUser> implements LyUserService {

    @Resource
    LyUserMapper lyUsermapper;

    @Resource
    EmailService emailService;

    @Resource
    RedisUtils redisUtils;

    @Resource
    LyUserRoleMapper lyUserRolemapper;

    @Resource
    AuthenticationProvider authenticationProvider;

    @Resource
    JwtUtils jwtUtils;


    @Resource
    LyRoleMapper lyRoleMapper;


    /**
     * 邮箱注册-发送验证码
     */
    @Override
    public void SendEmailVerificationCode(String Email) throws LyException {

        /**
         * 邮箱验证
         * */
        if (!RegexVerification.validEmail(Email)) {
            throw new LyException("邮箱格式不正确！", ErrorCode.EMAIL_REGEX_VERIFICATION_FAILED);
        }
        /**
         * 构建验证码
         * */
        Integer code = new Random().nextInt(9000) + 1000;

        /**
         * 判断用户是否存在
         */
        Map<String, Object> params = new HashMap<>();
        params.put("userName", Email);
        List<LyUser> ArrListUser = lyUsermapper.getLyUserListByMap(params);
        if (StringUtils.isNotEmpty(ArrListUser)) {
            throw new LyException("用户已存在", ErrorCode.AUTH_USER_ALREADY_EXISTS);
        }

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<html>");
        stringBuffer.append("<body>");
        stringBuffer.append("<h1>");
        stringBuffer.append("欢迎注册凌云系统，您的验证码为：<a style=\"color: red\">").append(code).append("</a>,有效时间3分钟!（若本人无相关操作,请忽略本邮件）");
        stringBuffer.append("</h1>");
        stringBuffer.append("</body>");
        stringBuffer.append("</html>");
        System.out.println(stringBuffer);
        /**
         * 发送验证码
         * */
        emailService.Email(Email, stringBuffer.toString());

        /**
         * 存入redis中
         * */
        String key = "activation_mobile:" + Email;
        redisUtils.setCacheObject(key, code, 60 * 3, TimeUnit.SECONDS);
    }

    /**
     * 注册-数据入库
     */
    @Override
    public void register(UserRegister userRegister) throws LyException {

        // 校验注册数据
        if (StringUtils.isNull(userRegister)) {
            throw new LyException("请传递参数！", ErrorCode.AUTH_PARAMETER_ERROR);
        }
        if (StringUtils.isEmpty(userRegister.getPassword())) {
            throw new LyException("请传递用户密码！", ErrorCode.AUTH_PARAMETER_ERROR);
        }

//        /**
//         * 邮箱验证
//         * */
//        if (!RegexVerification.validEmail(userRegisterVO.getUserName())) {
//            throw new LyException("邮箱格式不正确！", ErrorCode.EMAIL_REGEX_VERIFICATION_FAILED);
//        }

        // 判断用户是否存在
        Map<String, Object> params = new HashMap<>();
        params.put("userName", userRegister.getUserName());
        List<LyUser> lyUserList = lyUsermapper.getLyUserListByMap(params);
        if (StringUtils.isNotEmpty(lyUserList)) {
            throw new LyException("用户已存在！", ErrorCode.AUTH_USER_ALREADY_EXISTS);
        }

//        /**
//         *
//         *验证数据库 密码是否有效
//         *
//         * */
//        String key = "activation_mobile:" + userRegisterVO.getUserName();
//        if (!redisUtils.exist(key)) {
//            throw new LyException("验证码过期，请重新发送！", ErrorCode.EMAIL_VERIFICATION_CODE_INVALID);
//        }
//        /**
//         *
//         * 判断验证码是否正确
//         *
//         * */
//        if (!redisUtils.get(key).equals(userRegisterVO.getVerificationCode())) {
//            throw new LyException("验证码输入错误！", ErrorCode.EMAIL_VERIFICATION_CODE_INVALID);
//        }



        // 构建用户信息
        LyUser lyUser = new LyUser();
        BeanUtils.copyProperties(userRegister, lyUser);
        // 设置账号昵称
        lyUser.setNickName(userRegister.getUserName());

        // 密码加密  使用Security
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String ss = passwordEncoder.encode(lyUser.getPassword());
        lyUser.setPassword(ss);

        // 数据入库
        lyUsermapper.addUser(lyUser);

        // 用户和校色关系便 添加
        LyUserRole lyUserRole = new LyUserRole();
        lyUserRole.setUserId(lyUser.getUserId());

        //默认用户角色为 普通角色
        lyUserRole.setRoleId(2L);
        lyUserRolemapper.add(lyUserRole);
    }

    /**
     * 登录
     * @param loginFrom
     * @param request
     * @return
     * @throws CaptchaExpireException
     * @throws CaptchaException
     * @throws UserPasswordNotMatchException
     * @throws ServiceException
     * @throws UserAuthenticationException 用户不存在/密码错误
     */
    @Override
    public String login(LoginFrom loginFrom, HttpServletRequest request) throws CaptchaExpireException, CaptchaException, UserPasswordNotMatchException, ServiceException, UserAuthenticationException {

        // 校验验证码（验证码）
        validateCaptcha(loginFrom.getVerificationCode(), loginFrom.getKeyCode());

        // 登录（身份验证）
        Authentication authentication = null;
        try {
            authentication = authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(loginFrom.getUserName(), loginFrom.getPassword()));
        } catch (Exception e) {
            if (e instanceof BadCredentialsException) { // 密码错误
                throw new UserPasswordNotMatchException(e.getMessage());
            } else if (e instanceof UsernameNotFoundException) {
                throw new UserAuthenticationException(e.getMessage()); // 1、用户不存在/密码错误 2、该用户未拥有角色，无法登录
            } else {
                throw new ServiceException(e.getMessage());
            }
        }

        LoginUser loginUser = (LoginUser) authentication.getPrincipal();

        return jwtUtils.createToken(loginUser);

    }

    /**
     * 校验验证码
     *
     * @param verificationCode 验证码
     * @param keyCode          唯一标识
     */
    public void validateCaptcha(String verificationCode, String keyCode) throws CaptchaExpireException, CaptchaException {
        String captcha = redisUtils.getCacheObject(keyCode);
        if (captcha == null) {
            throw new CaptchaExpireException();
        }
        if (!verificationCode.equals(captcha)) {
            throw new CaptchaException();
        }
        redisUtils.delete(keyCode);
    }






    /*--------------------------------------------------------------------------------------------------------------------------------------------------*/


    @Override
    public IPage<UserAll> getUserList(UserQueryCriteria userQueryCriteria) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (!StrUtil.hasEmpty(userQueryCriteria.getNickName())) {
            queryWrapper.like("nick_name", userQueryCriteria.getNickName());
        }
        if (!StrUtil.hasEmpty(userQueryCriteria.getUserName())) {
            queryWrapper.like("user_name", userQueryCriteria.getUserName());
        }
        if (!StrUtil.hasEmpty(userQueryCriteria.getLoginIp())) {
            queryWrapper.like("user_ip", userQueryCriteria.getLoginIp());
        }
        queryWrapper.orderByAsc("user_id");
        Page<UserAll> page = new Page<>(userQueryCriteria.getCurrent(), userQueryCriteria.getSize());
        IPage<UserAll> iPage = lyUsermapper.getUserList(page, queryWrapper);
        return iPage;
    }

    @Transactional
    @Override
    public boolean addUser(UserFrom userFrom) throws LyException {

//        //邮箱验证
//        if (!RegexVerification.validEmail(userFrom.getUserEmail())) {
//            throw new LyException("邮箱格式不正确！", ErrorCode.EMAIL_REGEX_VERIFICATION_FAILED);
//        }
        // 判断用户是否存在
        Map<String, Object> params = new HashMap<>();
        params.put("userName", userFrom.getUserName());
        List<LyUser> lyUserList = lyUsermapper.getLyUserListByMap(params);
        if (StringUtils.isNotEmpty(lyUserList)) {
            throw new LyException("用户已存在，请更换账号！", ErrorCode.AUTH_USER_ALREADY_EXISTS);
        }

        LyUser lyUser = new LyUser();
//        lyUser.setUserName(userFrom.getUserName());
//        lyUser.setNickName(userFrom.getNickName());
//        lyUser.setUserType(userFrom.getUserType());
//        lyUser.setUserEmail(userFrom.getUserEmail());
//        lyUser.setPhonenumber(userFrom.getPhonenumber());
//        lyUser.setUserSex(userFrom.getUserSex());
//        lyUser.setAvatar(userFrom.getAvatar());


        // 源对象， 目标对象
        BeanUtils.copyProperties(userFrom,lyUser);

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String ss = passwordEncoder.encode(userFrom.getPassword());
        lyUser.setPassword(ss);

        lyUser.setUserStatus(userFrom.getUserStatus());
        lyUser.setDelFlag("0");

        //用户数据入库
        lyUsermapper.addUser(lyUser);
        //用户和角色关系入库
        Long[] roleids = userFrom.getRoleIds();
        if (StringUtils.isNotNull(roleids)) {
            List<LyUserRole> lyUserRoleList = new ArrayList<LyUserRole>();
            for (Long roleid : roleids) {
                LyUserRole lyUserRole = new LyUserRole();
                lyUserRole.setUserId(lyUser.getUserId());
                lyUserRole.setRoleId(roleid);
                lyUserRoleList.add(lyUserRole);
            }
            if (lyUserRoleList.size() > 0) {
                lyUserRolemapper.addlist(lyUserRoleList);
            }
        }
        return true;
    }

    @Override
    public UserFrom selectUserById(Long userId) {
        return lyUsermapper.selectUserById(userId);
    }


    @Override
    public List<Long> getUserRole(Long userId) {
        return lyRoleMapper.getUserRole(userId);
    }

    @Override
    public Boolean updateUser(UserFrom userFrom) {

        LyUser lyUser = new LyUser();
        lyUser.setUserId(userFrom.getUserId());
        lyUser.setUserName(userFrom.getUserName());
        lyUser.setNickName(userFrom.getNickName());
        lyUser.setUserType(userFrom.getUserType());
        lyUser.setUserEmail(userFrom.getUserEmail());
        lyUser.setPhonenumber(userFrom.getPhonenumber());
        lyUser.setUserSex(userFrom.getUserSex());
        lyUser.setAvatar(userFrom.getAvatar());
        lyUser.setUserStatus(userFrom.getUserStatus());

        // 修改用户表
        lyUsermapper.updateUser(lyUser);

        // 删除原先的关系
        lyUserRolemapper.DeleteId(userFrom.getUserId());
        // 用户和角色关系入库
        Long[] roleids = userFrom.getRoleIds();
        if (StringUtils.isNotNull(roleids)) {
            List<LyUserRole> lyUserRoleList = new ArrayList<LyUserRole>();
            for (Long roleid : roleids) {
                LyUserRole lyUserRole = new LyUserRole();
                lyUserRole.setUserId(lyUser.getUserId());
                lyUserRole.setRoleId(roleid);
                lyUserRoleList.add(lyUserRole);
            }
            if (lyUserRoleList.size() > 0) {
                lyUserRolemapper.addlist(lyUserRoleList);
            }
        }
        return true;
    }

    @Override
    public List<UserAll> userExportExcel(UserQueryCriteria userQueryCriteria) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (!StrUtil.hasEmpty(userQueryCriteria.getNickName())) {
            queryWrapper.like("nick_name", userQueryCriteria.getNickName());
        }
        if (!StrUtil.hasEmpty(userQueryCriteria.getUserName())) {
            queryWrapper.like("user_name", userQueryCriteria.getUserName());
        }
        if (!StrUtil.hasEmpty(userQueryCriteria.getLoginIp())) {
            queryWrapper.like("login_ip", userQueryCriteria.getLoginIp());
        }
        queryWrapper.orderByAsc("user_id");
        return lyUsermapper.userExportExcel(queryWrapper);
    }

    @Override
    public IPage<UserAll> authorizationUserList(AllocationUserCondition allocationUserCondition) {

        QueryWrapper queryWrapper = new QueryWrapper();
        if (!StrUtil.hasEmpty(allocationUserCondition.getNickName())) {
            queryWrapper.like("u.nick_name", allocationUserCondition.getNickName());
        }
        if (!StrUtil.hasEmpty(allocationUserCondition.getUserName())) {
            queryWrapper.like("u.user_name", allocationUserCondition.getUserName());
        }
//        if (allocationUserCondition.getRoleId() !=null) {
//            queryWrapper.eq("r.role_id", allocationUserCondition.getRoleId());
//        }
        queryWrapper.orderByAsc("r.role_id", "u.user_id");
        Page<UserAll> page = new Page<>(allocationUserCondition.getCurrent(), allocationUserCondition.getSize());
        IPage<UserAll> iPage = lyUsermapper.authorizationUserList(page, queryWrapper);
        return iPage;
    }


}
