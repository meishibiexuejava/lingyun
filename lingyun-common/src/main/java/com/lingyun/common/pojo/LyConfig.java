package com.lingyun.common.pojo;


import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * (LyConfig)
 *
 * @author 没事别学JAVA
 * @since 2024-04-23 18:25:27
 */
@Data
@ToString
@TableName(value = "ly_config")
@ApiModel(value = "LyConfig对象",description = "(LyConfig)")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LyConfig implements Serializable {

    /**
     *主键
     */
    @TableId(value = "config_id",type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long configId;

    /**
     *名称
     */
    @ApiModelProperty(value = "名称")
    private String configNanme;

    /**
     *键名
     */
    @ApiModelProperty(value = "键名")
    private String configKey;

    /**
     *键值
     */
    @ApiModelProperty(value = "键值")
    private String configValue;

    /**
     *是否开启（Y是 N否）
     */
    @ApiModelProperty(value = "是否开启（Y是 N否）")
    private String configType;

    /**
     *备注
     */
    @ApiModelProperty(value = "备注")
    private String configRemark;
}
