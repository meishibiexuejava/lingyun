package com.lingyun.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * 菜单信息
 *
 * @author 没事别学JAVA
 * 2021-11-10 17:10:28
 */
@Data
@ToString
@ApiModel(value = "LyMenu", description = "菜单信息")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LyMenu implements Serializable {

    /**
     * 菜单id
     */
    @ApiModelProperty(value = "菜单id", example = "null")
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Long menuId;

    /**
     * 父编号
     */
    @ApiModelProperty(value = "父编号", required = true,example = "0")
    private Long parentId;

    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称", required = true,example = "测试菜单")
    private String menuName;

    /**
     * 路由地址
     */
    @ApiModelProperty(value = "路由地址",example = "/test")
    private String path;

    /**
     * 排序字段（越小越靠前）
     */
    @ApiModelProperty(value = "排序字段（越小越靠前）",example="7")
    private Long sort;

    /**
     * 菜单状态（1显示 0隐藏）
     */
    @ApiModelProperty(value = "菜单状态（1显示 0隐藏）",example = "1")
    private String visible;

    /**
     * 菜单图标
     */
    @ApiModelProperty(value = "菜单图标",example="el-icon-success")
    private String icon;

    /**
     * 菜单类型（ A 目录 B 菜单 C 按钮 ）
     */
    @ApiModelProperty(value = "菜单类型（ A 目录 B 菜单 C 按钮 ）",example = "A")
    private String menuType;

    /**
     * 打开方式（tab页签 new新窗口）
     */
    @ApiModelProperty(value = "打开方式（tab页签 new新窗口）",example="tab")
    private String target;

    /**
     * 权限
     */
    @ApiModelProperty(value = "权限")
    private String perms;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",hidden = true)
    private Date updateTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注",example="测试备注")
    private String remarks;

    /**
     * 附加属性
     * 子菜单
     */
    @TableField(exist = false)
    @ApiModelProperty(hidden = true)
    private List<LyMenu> children = new ArrayList<LyMenu>();

}
