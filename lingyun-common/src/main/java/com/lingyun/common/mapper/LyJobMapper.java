package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.pojo.LyJob;

/**
 * 定时任务调度表(LyJob)
 *
 * @author 没事别学JAVA
 * @since 2023-01-02 20:43:44
 */
public interface LyJobMapper extends BaseMapper<LyJob> {

    /**
     * 根据条件分页查询定时任务列表
     * @param page 分页
     * @return 结果
     */
    public IPage<LyJob> quartzList(IPage<LyJob> page);


    /**
     * 单个查询
     *
     * @param jobId 任务ID
     * @return 结果
     */
    public LyJob queryById(Long jobId);


    /**
     * 添加定时任务
     * @param lyJob 定时任务信息
     * @return 结果
     */
    public Integer insertLyJob(LyJob lyJob);


    /**
     * 选择性插入
     *
     * @param lyJob 定时任务调度表
     * @return 结果
     */
    public Integer insertSelective(LyJob lyJob);


    /**
     * 删除定时任务
     * @param jobId 定时任务id
     * @return 结果
     */
    public Integer deleteById(Long jobId);


    /**
     * 通过主键修改数据
     *
     * @param lyJob 定时任务调度表
     * @return 结果
     */
    public Integer updateLyJob(LyJob lyJob);


    /**
     * 修改定时任务
     * @param lyJob 定时任务信息
     * @return 结果
     */
    public Integer updateSelective(LyJob lyJob);









}
