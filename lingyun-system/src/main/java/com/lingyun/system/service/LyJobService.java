package com.lingyun.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lingyun.common.corebase.GeneralEntity;
import com.lingyun.common.exception.quartz.QuartzInOperationException;
import com.lingyun.common.pojo.JobFrom;
import com.lingyun.common.pojo.LyJob;
import com.lingyun.common.vo.quartz.QuartzSearch;
import com.lingyun.common.vo.quartz.QuartzStatus;
import org.quartz.SchedulerException;

/**
 * 定时任务调度表(LyJob)表服务接口
 *
 * @author 没事别学JAVA
 * @since 2023-01-02 20:58:26
 */
public interface LyJobService extends IService<LyJob> {

    /**
     * 根据条件分页查询定时任务列表
     * @param quartzSearch 定时任务筛选条件
     * @return 结果
     */
    IPage<LyJob> quartzList(QuartzSearch quartzSearch);


    /**
     * 添加定时任务
     * @param job 定时任务信息
     * @throws SchedulerException 异常
     */
    public void quartzAdd(LyJob job) throws SchedulerException;


    /**
     * 修改定时任务
     * @param job 定时任务信息
     * @return 结果
     */
    public Integer quartzUpdate(LyJob job);


    /**
     * 添加定时任务(入库)
     * @param job 定时任务信息类
     * @return 结果
     */
    public Integer insertScheduledTask(LyJob job);


    /**
     * 删除定时任务
     * @param jobId 定时任务id
     * @return 结果
     */
    public Integer quartzDelete(Long jobId) throws SchedulerException, QuartzInOperationException;

    /**
     * 修改定时任务状态
     * @param quartzStatus 定时任务信息
     * @return 结果
     */
    public Integer updateJobStatus(QuartzStatus quartzStatus);


}
