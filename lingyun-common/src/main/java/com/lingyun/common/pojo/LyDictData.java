package com.lingyun.common.pojo;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * (LyDictData)表实体类
 *
 * @author 没事别学JAVA
 * @since 2022-07-12 21:13:15
 */
@Data
@ToString
@ApiModel(value = "LyDictData", description = "字典数据信息")
@TableName(value = "ly_dict_data")
public class LyDictData implements Serializable {

    /**
     *字典编码
     */
    @TableId(value = "dict_code",type = IdType.AUTO)
    @ApiModelProperty(value = "字典编码", example = "null")
    private Long dictCode;
    /**
     *字典排序
     */
    @ApiModelProperty(value = "字典排序")
    private Integer dictSort;
    /**
     *字典标签
     */
    @ApiModelProperty(value = "字典标签", required = true)
    private String dictLabel;
    /**
     *字典键值
     */
    @ApiModelProperty(value = "字典键值", required = true)
    private String dictValue;
    /**
     *字典类型
     */
    @ApiModelProperty(value = "字典类型", required = true)
    private String dictType;
    /**
     *样式属性（其他样式扩展）
     */
    @ApiModelProperty(value = "样式属性")
    private String cssClass;
    /**
     *表格回显样式
     */
    @ApiModelProperty(value = "表格回显样式",example = "default")
    private String listClass;
    /**
     *状态（0正常 1停用）
     */
    @ApiModelProperty(value = "状态", example = "0")
    private String status;
    /**
     *创建时间
     */
    @ApiModelProperty(value = "创建时间", hidden = true)
    private Date createTime;
    /**
     *更新时间
     */
    @ApiModelProperty(value = "更新时间", hidden = true)
    private Date updateTime;
    /**
     *备注
     */
    @ApiModelProperty(value = "备注", example = "备注")
    private String remark;
}
