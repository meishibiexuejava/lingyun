package com.lingyun.common.vo.menu;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

/**
 *  左侧导航栏-子路由显示信息
 */
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Meta {
    /**
     * 设置该路由在侧边栏和面包屑中展示的名字
     */
    private String title;

    /**
     * 设置该路由的图标，对应路径src/assets/icons/svg
     */
    private String icon;
}
