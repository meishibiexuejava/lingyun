package com.lingyun.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lingyun.common.corebase.GeneralEntity;
import com.lingyun.common.pojo.LyRole;
import com.lingyun.common.pojo.LyUser;
import com.lingyun.common.vo.role.RoleVagueQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色信息表(LyRole)表数据库访问层
 *
 * @author 没事别学JAVA
 * @since 2021-11-24 00:15:47
 */
public interface LyRoleMapper extends BaseMapper<LyRole> {


    /**
     * 根据用户id 获取当前用户的角色列表
     *
     * @param userid 用户id
     * @return
     */
    public List<Long> getUserRole(Long userid);


    /**
     * 根据条件分页查询角色列表
     *
     * @param page          分页
     * @param queryWrapper  条件
     * @param generalEntity 拼接sql(数据权限)
     * @return 角色列表
     */
    public IPage<LyRole> roleList(IPage<LyRole> page, @Param("ew") Wrapper<LyRole> queryWrapper, @Param("ly") GeneralEntity generalEntity);

    /**
     * 根据角色ID查询角色信息
     *
     * @param roleId 角色ID
     * @return 角色信息
     */
    public LyRole selectRoleById(Long roleId);

    /**
     * 导出角色Excel信息-也可添加条件
     */
    public List<LyRole> roleExportExcel(@Param("ew") Wrapper<LyRole> queryWrapper);


    /**
     * 根据角色id分页查询用户
     *
     * @param page         分页
     * @param queryWrapper 条件
     * @return 用户集合
     */
    public IPage<LyUser> selectRoleUser(IPage<LyUser> page, @Param("ew") Wrapper<LyUser> queryWrapper);


}
