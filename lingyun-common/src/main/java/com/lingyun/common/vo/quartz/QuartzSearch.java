package com.lingyun.common.vo.quartz;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lingyun.common.corebase.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * @author meishibiexuejava
 * 2023/4/23 0:14
 */
@Data
@ToString
@ApiModel(value = "QuartzSearch", description = "定时任务筛选条件")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QuartzSearch extends BaseEntity {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private Integer current;
    /**
     * 页大小
     */
    @ApiModelProperty(value = "页大小", required = true)
    private Integer size;

}
