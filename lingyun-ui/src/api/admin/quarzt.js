import request from '@/utils/request'


/**
 * 根据条件分页查询定时任务列表
 * @param query 查询条件
 * @returns {*}
 */
export function getQuartzList(query){

    return request({
        url: '/system/quartz/quartzList',
        method: 'get',
        params: query
    })

}

/**
 * 添加定时任务
 * @param data
 * @returns {*}
 */
export function quartzAdd(data){

    return request({
        url: '/system/quartz/quartzAdd',
        method: 'post',
        data:data
    })

}

/**
 * 根据id查询定时任务
 * @param jobId
 */
export function getQuartzById(jobId){

    return request({
        url: '/system/quartz/getQuartzById/'+ jobId,
        method: 'get',
    })

}


/**
 * 根据id修改定时任务
 * @param data
 */
export function quartzUpdate(data){

    return request({
        url: '/system/quartz/quartzUpdate',
        method: 'put',
        data: data,
    })

}

/**
 * 删除定时任务
 * @param jobId 定时任务id
 * @return 结果
 */
export function quartzDelete(jobId){

    return request({
        url: '/system/quartz/quartzDelete/'+jobId,
        method: 'delete',
    })

}

/**
 * 批量删除定时任务
 * @param ids 定时任务id 集合
 */
export function quartzIds(ids){

    return request({
        url: '/system/quartz/quartzIds',
        method: 'delete',
        data:ids
    })

}


/**
 * 修改定时任务状态
 * @param jobId  定时任务id
 * @param jobStatus 时任务状态
 * @returns {*}
 */
export function updateJobStatus(jobId, jobStatus) {

    const data = {
        jobId,
        jobStatus
    }
    return request({
        url: '/system/quartz/updateJobStatus',
        method: 'put',
        data: data
    })

}
