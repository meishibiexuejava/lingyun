package com.lingyun.common.utils.file;

import com.lingyun.common.config.LingYunConfig;

import java.io.File;
import java.io.IOException;

/**
 * 文件下载工具类
 *
 * @author 没事别学JAVA
 */
public class FileDownloadUtils {

    /**
     * 获取下载路径
     *
     * @param filename 文件名称
     */
    public static String getAbsoluteFile(String filename) {
        String downloadPath = LingYunConfig.getFilePath() +"/"+ filename;
        File desc = new File(downloadPath);
        if(!desc.exists()){
            if (!desc.getParentFile().exists()) {
                desc.getParentFile().mkdirs();
            }
        }
        return downloadPath;
    }

}
